(function ($) {
    $(document).on('click', '.save-category', function () {
        $('.alert').remove();
        $.fancybox.showLoading();
        var name = $('#name').val().trim();
        if (name === '') {
            $('#name').parent().parent().append('<br/><br/>' + error_notice);
            $.fancybox.hideLoading();
            return;
        }
        $.post(baseurl + 'categories/add-category', {name: name}, function (response) {
            window.location.href = baseurl + 'categories/active-categories';
        });
    });

    $(document).on('click', '.edit-category', function () {
        var id = this.id.replace('edit-', '');
        recordId = id;
        $('.save-category').addClass('save-edit-category');
        $('.save-category').removeClass('save-category');
        $('#name').val($('.cat-name-' + id).html().trim());
        $('.modal-title').html('Edit Category');
    });

    $(document).on('click', '.save-edit-category', function () {
        $('.alert').remove();
        $.fancybox.showLoading();
        var name = $('#name').val().trim();
        var id = recordId;
        if (name === '') {
            $('#name').parent().parent().append('<br/><br/>' + error_notice);
            $.fancybox.hideLoading();
            return;
        }

        $('.cat-name-' + id).html(name);
        $.post(baseurl + 'categories/edit-category/' + id, {name: name}, function (response) {
            console.log(response);
            $.fancybox.hideLoading();

            $('.close-modal').trigger('click');

            $('.save-edit-category').addClass('save-category');
            $('.save-category').removeClass('save-edit-category');
            $('#name').val('');
            $('.modal-title').html('Add Category');

        });

        $('.save-category').addClass('edit-category');
        $('.save-category').removeClass('save-category');
    });

    $(document).on('click', '.delete-category', function () {
        var id = this.id.replace('delete-', '');
        recordId = id;
        if (confirm('Are you sure you want to delete?')) {
             $.fancybox.showLoading();
            $.get(baseurl + 'categories/delete-category/' + id, {}, function (response) {
                console.log(response);
                if(response === '1'){
                    $.fancybox('Deleted successfully');
                    console.log($('.tr' + id).parent().parent());
                    $('.tr-' + id).remove();
                }else{
                    $.fancybox('Delete failed');
                }
                $.fancybox.hideLoading();
            });
        }
    });
    
     $(document).on('click', '.restore-category', function () {
        var id = this.id.replace('restore-', '');
        recordId = id;
             $.fancybox.showLoading();
            $.get(baseurl + 'categories/restore-category/' + id, {}, function (response) {
                console.log(response);
                if(response === '1'){
                    $.fancybox('Restored successfully');
                    console.log($('.tr' + id).parent().parent());
                    $('.tr-' + id).remove();
                }else{
                    $.fancybox('Restore failed');
                }
                $.fancybox.hideLoading();
            });
    });
})(jQuery);