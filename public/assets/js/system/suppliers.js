(function ($) {
    $(document).on('click', '.add-asset', function () {
        $.fancybox.showLoading();
        var id = this.id.replace('dispose-', '');
        recordId = id;
        $('.modal-body').html($('.s-add-form').html());
        $('.modal-title').html('Add Supplier');
        $.fancybox.hideLoading();
        $('.save-supplier-form').attr('action', baseurl + "suppliers/add-suppliers");
    });

    $(document).on('click', '.save-supplier-form', function (e) {
        e.preventDefault();
        status = validate();
        console.log(status);
        if (status == 0) {
            $.post(baseurl + 'suppliers/add-suppliers', {
                name: $('#name').val(),
                phone: $('#phone').val(),
                email: $('#email').val().trim(),
            }, function (response) {
                console.log(response);
                if (response === '1') {
                    var tr = ' <tr class="tr-' + response + '">'
                            + '<td>' + response + '</td>'
                            + '<td class="center name-' + response + '">' + $('#name').val() + '</td>'
                            + '<td class="center email-' + response + '">' + $('#email').val() + '</td>'
                            + '<td class="center phone-' + response + '">' + $('#phone').val() + '</td>'
                            + '<td>'
                            + '<a data-toggle="modal" href="#add" class="btn btn-primary btn-xs edit-supplier" id="edit-' + response + '"> Edit Supplier</a>'
                            + ' <a href="javascript:;" class="btn btn-danger btn-xs delete-supplier" id="delete-' + response + '">Delete Supplier</a>'
                            + '</td>'
                            + '</tr>';
                    $(".t-body").prepend(tr);
                    $('.close-modal').trigger('click');
                    $.fancybox('Saved Successfully');
                } else {
                    $.fancybox('Could not save, please retry');
                }
            });
        } else {
            return;
        }
    })


    $(document).on('click', '.edit-asset', function () {
        $.fancybox.showLoading();
        var id = this.id.replace('edit-', '');
        recordId = id;

        $('.modal-body').html($('.s-add-form').html());
        $('.modal-title').html('Edit Supplier');

        $.get(baseurl + 'suppliers/edit-supplier/' + id, {}, function (response) {
            console.log(response);
            $('#name').val(response.name);
            $('#email').val(response.email);
            $('#phone').val(response.phone);

            setTimeout(function () {
                $('.save-asset').removeClass('save-supplier-form');
                $('.save-asset').addClass('save-edit-supplier');
                $.fancybox.hideLoading();
            }, '1000');
        },'json');
    });

    $(document).on('click', '.save-edit-supplier', function (e) {
        e.preventDefault();
        status = validate();
        console.log(status);
        if (status == 0) {
            $.post(baseurl + 'suppliers/edit-supplier', {
                name: $('#name').val(),
                phone: $('#phone').val(),
                email: $('#email').val().trim(),
            }, function (response) {
                console.log(response);
                if (response === '1') {
                    $('.save-edit-supplier').addClass('save-supplier-form');
                    $('.save-supplier-form').removeClass('save-edit-supplier');
                    $('.close-modal').trigger('click');

                    $('.name-' + id).html($('#name').val());
                    $('.email-' + id).html($('#email').val());
                    $('.phone-' + id).html($('#phone').val());

                    $.fancybox('Saved Successfully');
                } else {
                    $.fancybox('Could not save, please retry');
                }
            });
        } else {
            return;
        }
    })

    $(document).on('click', '.save-edit-asset', function (e) {
        e.preventDefault();
        $('.alert').remove();
        $.fancybox.showLoading();

        var req = $('input,textarea,select').filter('[required]:visible');
        errors = 0;
        $(req).each(function (datakey, datavalue) {
            data = datavalue.value;
            if (data.length === 0) {
                $("#" + datavalue.id).parent().append('<br/><br/>' + error_notice);
                errors = 1;
            }
        });

        if (errors == 1) {
            $.fancybox.hideLoading();
            return;
        }

        var name = $('#name').val().trim();
        var category_label = $('#category_id option:selected').text();
        var status_label = $('#status option:selected').text();

        var category_id = $('#category_id').val().trim();
        var status = $('#status').val().trim();
        var note = $('#note').val().trim();
        var amount = $('#price').val().trim();

        var id = recordId;

        console.log($('#price').val().trim());
        $.post(baseurl + 'assets/edit-asset/' + id,
                {
                    name: name,
                    category_id: category_id,
                    status: status,
                    amount: amount,
                    note: note
                }, function (response) {
            console.log(response);
            $.fancybox.hideLoading();
            $('.close-modal').trigger('click');
            $('.save-edit-asset').addClass('save-asset');
            $('.save-asset').removeClass('save-edit-asset');
            $('.modal-title').html('Add Category');
            $('.name-' + id).html(name);
            $('.value-' + id).html(amount);
            $('.category-' + id).html(category_label);
            $('.status-' + id).html(status_label);

            $('.close-modal').trigger('click');
            $.fancybox('Saved Successfully');
        });

        $('.save-category').addClass('edit-category');
        $('.save-category').removeClass('save-category');
    });

    $(document).on('click', '.delete-asset', function () {
        var id = this.id.replace('delete-', '');
        recordId = id;
        if (confirm('Are you sure you want to delete?')) {
            $.fancybox.showLoading();
            $.get(baseurl + 'suppliers/delete-supplier/' + id, {}, function (response) {
                console.log(response);
                if (response === '1') {
                    $.fancybox('Deleted successfully');
                    console.log($('.tr' + id).parent().parent());
                    $('.tr-' + id).remove();
                } else {
                    $.fancybox('Delete failed');
                }
                $.fancybox.hideLoading();
            });
        }
    });

    $(document).on('click', '.restore-asset', function () {
        var id = this.id.replace('restore-', '');
        recordId = id;
        $.fancybox.showLoading();
        $.get(baseurl + 'suppliers/restore-supplier/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                $.fancybox('Restored successfully');
                console.log($('.tr' + id).parent().parent());
                $('.tr-' + id).remove();
            } else {
                $.fancybox('Restore failed');
            }
            $.fancybox.hideLoading();
        });
    });

    $(document).on('click', '.dispose-asset', function () {
        $.fancybox.showLoading();
        var id = this.id.replace('dispose-', '');
        recordId = id;
        $('.modal-body').html($('.disposal').html());
        $('.modal-title').html('Dispose Asset');
        $.fancybox.hideLoading();
    });

    $(document).on('change', '#disposemethod', function () {
        $.fancybox.showLoading();
        method = $(this).val();

        if (method == 2) {
            $('#amount').attr('placeholder', 'Price at which the item will be sold');
        } else {
            $('#amount').attr('placeholder', 'Cost that will be incurred');
        }
        $.fancybox.hideLoading();
    });


    $(document).on('click', '.save-dispose-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        console.log(baseurl + 'assets/dispose-asset/' + recordId);
        $.get(baseurl + 'assets/dispose-asset/' + recordId,
                {
                    'disposemethod': $('#disposemethod').val(),
                    'amount': $('#amount').val(),
                    'note': $('#note').val(),
                }
        , function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Disposed successfully');
                $('.tr-' + recordId).remove();
            } else {
                $.fancybox('Dispose failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.no-dispose-asset', function () {
        var id = $(this).data('id');
        recordId = id;
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/delete-disposable-asset/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                $.fancybox('Restored successfully');
                console.log($('.tr' + id).parent().parent());
                $('.tr-' + id).remove();
            } else {
                $.fancybox('Restore failed');
            }
            $.fancybox.hideLoading();
        });
    });


    $(document).on('click', '.mark-diposed', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        $('.modal-body').html($('.disposal-details').html());
        $('.modal-title').html('Mark As Disposed');
        $.fancybox.hideLoading();
    });

    $(document).on('click', '.save-disposed', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        status = validate();
        console.log(status);
        if (status == 0) {
            $.post(baseurl + 'assets/edit-diposed/' + recordId,
                    {
                        names: $('#names').val(),
                        email: $('#email').val(),
                        phone_no: $('#phone').val(),
                        note: $('#note').val(),
                        status: 2
                    }, function (response) {
                console.log(response);
                if (response === '1') {
                    $.fancybox('Updated successfully');
                    $('.status-' + recordId).html('Disposed');
                    $('.close-modal').trigger('click');
                } else {
                    $.fancybox('Update failed');
                }
                $.fancybox.hideLoading();
            });
        } else {
            $.fancybox.hideLoading();
            return;
        }
        $.fancybox.hideLoading();
    });
//    $(document).on('click', '.mark-diposed', function () {
//        var id = $(this).data('id');
//        recordId = id;
//        $.fancybox.showLoading();
//        $.get(baseurl + 'assets/edit-diposed/' + id, {status: 2}, function (response) {
//            console.log(response);
//            if (response === '1') {
//                $.fancybox('Updated successfully');
//                $('.status-' + id).html('Disposed');
//            } else {
//                $.fancybox('Update failed');
//            }
//            $.fancybox.hideLoading();
//        });
//    });

    $(document).on('click', '.assign-asset', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Assign ' + assetname);
        $.fancybox.hideLoading();
    });

    $(document).on('click', '.save-assign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/assign-asset/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Assigned successfully');
                $('.tr-' + recordId).remove();
            } else {
                $.fancybox('Assign failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.reassign-asset', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Reassign ' + assetname);
        $.fancybox.hideLoading();
        $('.save-assign-asset').addClass('save-reassign-asset');
        $('.save-assign-asset').removeClass('save-assign-asset');

    });

    $(document).on('click', '.save-reassign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/edit-assign/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Reassigned successfully');
            } else {
                $.fancybox('Reassigned failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });
})(jQuery);