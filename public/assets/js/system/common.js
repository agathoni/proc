(function ($) {
    baseurl = 'http://localhost/proc/public/';
    error_notice = '<div class="alert alert-dismissable alert-danger"> <strong>Oh snap!</strong>This field is required.<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>';

    $(document).on('click', '.btn-cancel', function (e) {
        e.preventDefault();
        $.fancybox.hideLoading();
        $('.form').trigger("reset");
        $.fancybox.close();
    });
//
//    $(document).on('click', '.close-modal', function (e) {
//        $('.close-modal').trigger('click');
//        $.fancybox.close();
//    });

    $(window).load(function () {
        $(".table").dataTable({
            "aaSorting": [[0, "desc"]],
            "bDestroy": true
        });
    })
})(jQuery);

function timeout(time, action) {
    setTimeout(function () {
        action
    }, time);
}

function validate() {
    $('.alert').remove();
    var req = $('input,textarea,select').filter('[required]:visible');
    errors = 0;
    $(req).each(function (datakey, datavalue) {
        data = datavalue.value;
        if (data.length === 0) {
            $("#" + datavalue.id).parent().append('<br/>' + error_notice);
            errors = 1;
        } else {
            $("#" + datavalue.id).parent().find('.alert').remove();
        }
    });

    if ($('#phone').length)
    {
        if (validatePhone($('#phone').val().trim(), '#phone') !== 'valid') {
            errors = 1;
        }
    }

    if (!validateEmail($("#email").val().trim())) {
        $("#email").parent().append('<br/><div class="alert alert-dismissable alert-danger"> <strong>Oh snap!</strong>Invalid email</div>');
        errors = 1;
    }

    return errors;
}


function validatePhone(telephone, field) {
    console.log(telephone);

    $(field).parent().find('.errorMessage').remove();
    $(field).parent('.errorMessage').remove();
    var status = 'valid';
    first = telephone.substring(0, 1);
    second = telephone.substring(0, 2);
    third = telephone.substring(0, 3);
    fourth = telephone.substring(0, 4);
    if (telephone.length === 9) {
        if (second === '07') {
            status = 'short';
        } else if (second === '254') {
            status = 'short';
        } else if (first == '7') {
            status = 'valid';
        } else {
            status = 'invalid';
        }
    } else if (telephone.length === 10 && second !== '07' && third !== '254') {
        status = 'invalid';
    } else if (telephone.length === 12) {
        if (fourth === '2540') {
            status = 'short';
        } else if (third !== '254') {
            status = 'invalid';
        }
    } else if (telephone.length === 13) {
        if (fourth !== '2540') {
            status = 'invalid';
        } else if (third !== '+25' && third !== '254') {
            status = 'invalid';
        }
    } else if (telephone.length === 11) {
        status = 'invalid';
    } else if (telephone.length > 14) {
        status = 'invalid';
    } else if (telephone.length < 9) {
        status = 'short';
    }

    if (status !== 'valid') {
        $(field).parent().append('<br/><div class="alert alert-dismissable alert-danger"> <strong>Oh snap!</strong>Invalid phone number</div>');
    }

    return status;
}


function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        return false;
    } else {
        return true;
    }
}

$(document).on('click', '.save-form', function (e) {
    e.preventDefault();
    status = validate();
    console.log(status);
    if (status == 0) {
        $('.form-horizontal').submit();
    } else {
        return;
    }
})