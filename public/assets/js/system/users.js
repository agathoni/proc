(function ($) {
    var baseurl = 'http://localhost/proc/public/';
    window.edit_form = $(document).find('.edit-user-form').find('div:first').clone(true);
    $(document).find('.edit-user-form').remove();

    $(document).on('click', '.edit-user', function () {
        $.fancybox.showLoading();
        var invoice = this.id.trim();
        id_array = invoice.split('-');
        id = inquiry_array[1];
        name = inquiry_array[2];

        console.log(id_array);

        $.fancybox(window.edit_form, {modal: true});
    });

    $(document).on('click', '.delete-user', function () {
        var id = this.id.trim().replace('delete-', '');
        $.get(baseurl + 'users/delete/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                console.log('asdasd');
                $('#delete-' + id).parent().parent().remove();
                $.fancybox('Record Deleted Successfully');
            } else {
                $.fancybox('Sorry could not delete please retry');
            }
        });
    });

    $(document).on('click', '.restore-user', function () {
        var id = this.id.trim().replace('restore-', '');
        $.get(baseurl + 'users/restore/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                $('#restore-' + id).parent().parent().remove();
                $.fancybox('Record Restored Successfully');
            } else {
                $.fancybox('Sorry could not restore please retry');
            }
        });
    });

    $(document).on('click', '.btn-cancel', function () {
        $.fancybox.close();
    });

//    $(document).on('click', '.save-user', function (e) {
//        e.preventDefault();
//        status = validaterequired();
//        console.log(status);
//        if (status == 0) {
//            $('.user-form').ajaxSubmit({
//                success: function (response) {
//                    console.log(response);
//                }
//            });
//        }
//    })
})(jQuery);
//$("#table").dataTable({
//    "aaSorting": [[0, "desc"]],
//    "bDestroy": true
//});