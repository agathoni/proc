(function ($) {

    $(document).on('click', '.assign-support', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Assign ' + assetname);
        $.fancybox.hideLoading();
    });

    $(document).on('click', '.save-assign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/assign-asset/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Assigned successfully');
                $('.tr-' + recordId).remove();
            } else {
                $.fancybox('Assign failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.reassign-asset', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Reassign ' + assetname);
        $.fancybox.hideLoading();
        $('.save-assign-asset').addClass('save-reassign-asset');
        $('.save-assign-asset').removeClass('save-assign-asset');

    });

    $(document).on('click', '.save-reassign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/edit-assign/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Reassigned successfully');
            } else {
                $.fancybox('Reassigned failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });
})(jQuery);