(function ($) {
    $(document).on('click', '.add-asset', function () {
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/add-asset', {}, function (response) {
            $('.modal-body').html(response);
            setTimeout(function () {
                $('.save-asset').addClass('ajax-save-asset')
            }, '1000');
            $.fancybox.hideLoading();
        });
        $('.add-asset').attr('action', baseurl + "assets/add-asset");
    });


    $(document).on('click', '.ajax-save-asset', function (e) {
        e.preventDefault();
        $('.alert').remove();
        $.fancybox.showLoading();
        var name = $('#name').val().trim();
        if (name === '') {
            $('#name').parent().append('<br/>' + error_notice);
            $.fancybox.hideLoading();
            return;
        }

        category = $('#category_id option:selected').text();
        status = $('#status option:selected').text();

        $('.add-asset-form').ajaxSubmit({
            success: function (resp) {
                if (typeof resp !== 'object')
                {
                    resp = jQuery.parseJSON(resp);
                }
                console.log(resp);
                response = resp[0];
                img = resp[1];
                console.log(response);

                var tr = ' <tr class="tr-' + response + '">'
                        + '<td>' + response + '</td>'
                        + '<td class="center name-' + response + '">' + name
                        + '<hr><img width="100px" src="' + baseurl + 'product_images/large/' + img + '">'
                        + '</td>'
                        + '<td class="center value-' + response + '">' + $('#amount').val() + '</td>'
                        + '<td class="center category-' + response + '">' + category + '</td>'
                        + '<td class="center status-' + response + '">' + status + '</td>'
                        + '<td>'
                        + '<a data-toggle="modal" href="#add" class="btn btn-primary btn-xs edit-asset" id="edit-' + response + '"> Edit Asset</a>'
                        + ' <a href="javascript:;" class="btn btn-danger btn-xs delete-asset" id="delete-' + response + '">Delete Asset</a>'
                        + ' <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs dispose-asset" id="dispose-' + response + '" data-price="' + $('#price').val() + '"> Dispose</a>'
                        + ' <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs assign-asset" data-id="' + response + '"> Asset</a>'
                        + '</td>'
                        + '</tr>';
                $(".t-body").prepend(tr)
                $('.close-modal').trigger('click');
                $.fancybox('Saved Successfully');
            }
        });
    });

    $(document).on('click', '.edit-asset', function () {
        $.fancybox.showLoading();
        var id = this.id.replace('edit-', '');
        recordId = id;
        var url = baseurl + 'assets/edit-asset/' + id;

        $.get(url, {}, function (response) {
            $('.modal-body').html(response);
            $('.modal-title').html('Edit Asset');
            setTimeout(function () {
                $.get(baseurl + 'assets/asset-data/' + id, {}, function (data) {
                    if (typeof data !== 'object')
                    {
                        data = jQuery.parseJSON(data);
                    }
                    data = data[0];
                    $('#name').val(data.name);
                    $('#amount').val(data.amount);
                    $('#note').val(data.note);
                    $("#category_id").val(data.category_id);
                    $("#status").val(data.status);
                    $('#units').val(data.units);
                    $('#bulkprice').val(data.bulkprice);
                    $('#supplier_id').val(data.supplier_id);

                    $.fancybox.hideLoading();
                });
                $('.add-asset').attr('action', url);
                $('.save-asset').removeClass('ajax-save-asset');
                $('.save-asset').addClass('save-edit-asset');
            }, '1000');
        });
    });

    $(document).on('click', '.save-edit-asset', function (e) {
        e.preventDefault();
        $('.alert').remove();
        $.fancybox.showLoading();
        var id = recordId;
        var req = $('input,textarea,select').filter('[required]:visible');
        errors = 0;
        $(req).each(function (datakey, datavalue) {
            data = datavalue.value;
            if (data.length === 0) {
                $("#" + datavalue.id).parent().append('<br/><br/>' + error_notice);
                errors = 1;
            }
        });

        if (errors == 1) {
            $.fancybox.hideLoading();
            return;
        }
        var name = $('#name').val().trim();
        var category_label = $('#category_id option:selected').text();
        var status_label = $('#status option:selected').text();

        var category_id = $('#category_id').val();
        var status = $('#status').val();
        var note = $('#note').val();
        var amount = $('#amount').val();

        $('.add-asset-form').ajaxSubmit({
            success: function (resp) {
                console.log(resp);
                if (typeof resp !== 'object')
                {
                    resp = jQuery.parseJSON(resp);
                }
                console.log(resp);
                response = resp[0];
                img = resp[1];
                console.log(response);
                $('.close-modal').trigger('click');
                $('.save-edit-asset').addClass('save-asset');
                $('.save-asset').removeClass('save-edit-asset');
                $('.modal-title').html('Add Category');
                $('.name-' + id).html(name + '<hr><img width="100px" src="' + baseurl + 'product_images/large/' + img + '">');
                $('.value-' + id).html(amount);
                $('.category-' + id).html(category_label);
                $('.status-' + id).html(status_label + '<hr> ' + note);
                $.fancybox('Saved Successfully');
            }
        });






//        $.post(baseurl + 'assets/edit-asset/' + id,
//                {
//                    name: name,
//                    category_id: category_id,
//                    status: status,
//                    amount: amount,
//                    note: note
//                }, function (response) {
//            console.log(response);
//            $.fancybox.hideLoading();
//            $('.close-modal').trigger('click');
//            $('.save-edit-asset').addClass('save-asset');
//            $('.save-asset').removeClass('save-edit-asset');
//            $('.modal-title').html('Add Category');
//            $('.name-' + id).html(name + '<hr><img width="100px" src="' + baseurl + 'product_images/large/' + img + '">');
//            $('.value-' + id).html(amount);
//            $('.category-' + id).html(category_label);
//            $('.status-' + id).html(status_label);
//
//            $('.close-modal').trigger('click');
//            $.fancybox('Saved Successfully');
//        });

        $('.save-category').addClass('edit-category');
        $('.save-category').removeClass('save-category');
    });

    $(document).on('click', '.delete-asset', function () {
        var id = this.id.replace('delete-', '');
        recordId = id;
        if (confirm('Are you sure you want to delete?')) {
            $.fancybox.showLoading();
            $.get(baseurl + 'assets/delete-asset/' + id, {}, function (response) {
                console.log(response);
                if (response === '1') {
                    $.fancybox('Deleted successfully');
                    console.log($('.tr' + id).parent().parent());
                    $('.tr-' + id).remove();
                } else {
                    $.fancybox('Delete failed');
                }
                $.fancybox.hideLoading();
            });
        }
    });

    $(document).on('click', '.restore-asset', function () {
        var id = this.id.replace('restore-', '');
        recordId = id;
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/restore-asset/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                $.fancybox('Restored successfully');
                console.log($('.tr' + id).parent().parent());
                $('.tr-' + id).remove();
            } else {
                $.fancybox('Restore failed');
            }
            $.fancybox.hideLoading();
        });
    });

    $(document).on('click', '.dispose-asset', function () {
        $.fancybox.showLoading();
        var id = this.id.replace('dispose-', '');
        recordId = id;
        $('.modal-body').html($('.disposal').html());
        $('.modal-title').html('Dispose Asset');
        $.fancybox.hideLoading();
    });

    $(document).on('change', '#disposemethod', function () {
        $.fancybox.showLoading();
        method = $(this).val();

        if (method == 2) {
            $('#amount').attr('placeholder', 'Price at which the item will be sold');
        } else {
            $('#amount').attr('placeholder', 'Cost that will be incurred');
        }
        $.fancybox.hideLoading();
    });


    $(document).on('click', '.save-dispose-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        console.log(baseurl + 'assets/dispose-asset/' + recordId);
        $.get(baseurl + 'assets/dispose-asset/' + recordId,
                {
                    'disposemethod': $('#disposemethod').val(),
                    'amount': $('#amount').val(),
                    'note': $('#note').val(),
                }
        , function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Disposed successfully');
                $('.tr-' + recordId).remove();
            } else {
                $.fancybox('Dispose failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.no-dispose-asset', function () {
        var id = $(this).data('id');
        recordId = id;
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/delete-disposable-asset/' + id, {}, function (response) {
            console.log(response);
            if (response === '1') {
                $.fancybox('Restored successfully');
                console.log($('.tr' + id).parent().parent());
                $('.tr-' + id).remove();
            } else {
                $.fancybox('Restore failed');
            }
            $.fancybox.hideLoading();
        });
    });


    $(document).on('click', '.mark-diposed', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        $('.modal-body').html($('.disposal-details').html());
        $('.modal-title').html('Mark As Disposed');
        $.fancybox.hideLoading();
    });

    $(document).on('click', '.save-disposed', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        status = validate();
        console.log(status);
        if (status == 0) {
            $.post(baseurl + 'assets/edit-diposed/' + recordId,
                    {
                        names: $('#names').val(),
                        email: $('#email').val(),
                        phone_no: $('#phone').val(),
                        note: $('#note').val(),
                        status: 2
                    }, function (response) {
                console.log(response);
                $.fancybox('Updated successfully');
                location.reload();
            });
        } else {
            $.fancybox.hideLoading();
            return;
        }
    });
//    $(document).on('click', '.mark-diposed', function () {
//        var id = $(this).data('id');
//        recordId = id;
//        $.fancybox.showLoading();
//        $.get(baseurl + 'assets/edit-diposed/' + id, {status: 2}, function (response) {
//            console.log(response);
//            if (response === '1') {
//                $.fancybox('Updated successfully');
//                $('.status-' + id).html('Disposed');
//            } else {
//                $.fancybox('Update failed');
//            }
//            $.fancybox.hideLoading();
//        });
//    });

    $(document).on('click', '.assign-asset', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Assign Asset');
        $.fancybox.hideLoading();
    });

    $(document).on('click', '.save-assign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/assign-asset/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Assigned successfully');
                $('#assign-' + recordId).html('Reassign');
                $('#assign-' + recordId).removeClass('assign-asset');
                $('#assign-' + recordId).addClass('reassign-asset');
            } else {
                $.fancybox('Assign failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.reassign-asset', function () {
        $.fancybox.showLoading();
        var id = $(this).data('id');
        recordId = id;
        assetname = $('.name-' + id).html();
        $('.modal-body').html($('.assign-form').html());
        $('.modal-title').html('Reassign ' + assetname);
        $.fancybox.hideLoading();
        $('.save-assign-asset').addClass('save-reassign-asset');
        $('.save-assign-asset').removeClass('save-assign-asset');

    });

    $(document).on('click', '.save-reassign-asset', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assets/edit-assign/' + recordId,
                {
                    'user_id': $('#assignuser_id').val(),
                }, function (response) {
            console.log(response);
            $('.close-modal').trigger('click');
            if (response === '1') {
                $.fancybox('Reassigned successfully');
            } else {
                $.fancybox('Reassigned failed');
            }
            $.fancybox.hideLoading();
        });
        $.fancybox.hideLoading();
        $('.modal-title').html('Add Asset');
    });

    $(document).on('click', '.create-support', function (e) {
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html($('.support-form').html());
    });

    $(document).on('click', '.save-support-issue', function (e) {
        e.preventDefault();
        $.fancybox.showLoading();
        $.get(baseurl + 'assetsupport/create-ticket',
                {
                    asset_id: id,
                    subject: $('#subject').val(),
                    message: $('#note').val()
                }, function (response) {
            console.log(response);
            $.fancybox.hideLoading();
            $('.close-modal').trigger('click');
        });
    });

})(jQuery);