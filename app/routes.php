<?php

Route::group(array('before' => 'auth'), function() {
    Route::get('/', 'DashController@index'); // to be replaced with dashboard

    Route::any('add-user', 'UsersController@addUser');
    Route::any('users', 'UsersController@allUsers');
    Route::any('edit-user/{user}', 'UsersController@editUser');
    Route::any('users/logs', function() {
        $view_data['content'] = 'users/user-logs';
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');
        return View::make('floor', $view_data);
    });
    Route::any('users/user-logs/{user}', function($user) {
        $view_data['content'] = 'users/user-logs';
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');
        $view_data['contentdata']['user'] = $user;
        $view_data['contentdata']['title'] = ' User Logs';

        $view_data['user'] = $user;
        return View::make('floor', $view_data);
    });
    Route::any('users/user-logs', function() {
        $view_data['content'] = 'users/user-logs';
        $view_data['contentdata']['js'] = array("assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        return View::make('floor', $view_data);
    });
    Route::any('users/delete/{id}', 'UsersController@delete');
    Route::any('edit-user/{user}', 'UsersController@editUser');
    Route::any('users/deleted-users', 'UsersController@deletedUsers');
    Route::any('users/restore/{id}', 'UsersController@restore');


    //Categories
    Route::any('categories/active-categories', 'CategoriesController@allCategories');
    Route::any('categories/add-category', 'CategoriesController@addCategory');
    Route::any('categories/edit-category/{id}', 'CategoriesController@editCategory');
    Route::any('categories/delete-category/{id}', 'CategoriesController@deleteCategory');
    Route::any('categories/restore-category/{id}', 'CategoriesController@restoreCategory');
    Route::any('categories/deleted-categories', 'CategoriesController@deletedcategories');

    //Assets
    Route::any('assets/active-assets', 'AssetsController@all');
    Route::any('assets/add-asset', 'AssetsController@add');
    Route::any('assets/edit-asset/{id}', 'AssetsController@edit');
    Route::any('assets/asset-data/{id}', 'AssetsController@data');
    Route::any('assets/delete-asset/{id}', 'AssetsController@delete');
    Route::any('assets/restore-asset/{id}', 'AssetsController@restore');
    Route::any('assets/deleted-assets', 'AssetsController@deleted');
    Route::any('assets/dispose-asset/{id}', 'AssetsController@dispose');
    Route::any('assets/disposed-assets', 'AssetsController@disposed');
    Route::any('assets/delete-disposable-asset/{id}', 'AssetsController@deletedisposable');
    Route::any('assets/edit-diposed/{id}', 'AssetsController@editDisposed');
    Route::any('assets/assign-asset/{id}', 'AssetsController@assign');
    Route::any('assets/edit-assign/{id}', 'AssetsController@editAssign');
    Route::any('assets/supplies', 'AssetsController@supplies');
    Route::any('assets/email-test/{asset_id}', 'AssetsController@emailtest');
    
    Route::any('assets/my-assets', 'ProfileController@myAssets');


    Route::any('suppliers/active-suppliers', 'SuppliersController@allSuppliers');
    Route::any('suppliers/add-suppliers', 'SuppliersController@addSupplier');
    Route::any('suppliers/edit-supplier/{id}', 'SuppliersController@editSupplier');
    Route::any('suppliers/supplier-data/{id}', 'SuppliersController@data');
    Route::any('suppliers/delete-supplier/{id}', 'SuppliersController@delete');
    Route::any('suppliers/restore-supplier/{id}', 'SuppliersController@restore');
    Route::any('suppliers/inactive-suppliers', 'SuppliersController@deletedSuppliers');
    
    Route::any('assetsupport/create-ticket/', 'SupportController@save');
    Route::any('assetsupport/all-messages', 'SupportController@supportIssues');
    
});


Route::any('login', 'AuthController@login');
Route::any('logout', function() {
    Auth::logout();
    Session::flash('success-message', 'Your are now logged out!');
    return Redirect::to('/');
});


Route::get('/updateapp', function() {
    \Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});

Route::filter('haspermission', function ($route, $request, $module) {
    if (!in_array($module, explode(',', Auth::user()->modules))) {
        Session::flash('error-message', 'You do not have permission to access this module.');
        return Redirect::to('/');
    }
}
);
