<?php

class ProfileController extends BaseController {

    public function myAssets() {
        $assets = new Assetsmodel();

        $view_data['contentdata']['title'] = 'My Assets';
        $view_data['contentdata']['caption'] = ' ';
        $view_data['contentdata']['assets'] = DB::table('assets')
                ->leftjoin('asset_categories', 'assets.category_id', '=', 'asset_categories.id')
                ->leftjoin('asset_status', 'assets.status', '=', 'asset_status.id')
                ->leftjoin('asset-assignment', 'assets.id', '=', 'asset-assignment.asset_id')
                ->select('asset-assignment.id as assignmentid', 'assets.image', 'assets.assigned', 'assets.id', 'assets.amount', 'assets.name', 'assets.note', 'asset_categories.name as category', 'asset_status.name as status')
                ->where('asset-assignment.user_id', Auth::user()->id)
                ->whereNull('assets.deleted_at')
                ->whereNull('assets.disposed')
                ->get();

        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assets/my-assets';
        $view_data['contentdata']['form-actions'] = ' ';

        $view_data['contentdata']['users'] = User::lists('name', 'id');

        self::logs('View my Assets');
        return View::make('floor', $view_data);
    }

    function logs($action) {
        $ulogs = new Userlog();

        if (Auth::check()) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => $action,
            );
        } else {
            $data = array(
                'user_id' => 0,
                'user_email' => 0,
                'action' => $action,
            );
        }
        $ulogs->fill($data);
        $ulogs->save();
    }
}
