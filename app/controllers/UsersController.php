<?php

class usersController extends BaseController {

    public function __construct() {
        $this->beforeFilter('haspermission:1');
    }

    public function addUser() {
        $ulogs = new Userlog();

        if (Request::method() == 'POST') {
            $data = Input::get();
            if (isset($data['permissions'])) {
                $data['modules'] = implode(',', $data['permissions']);
            }

            $user = new User();
            $user->password = Hash::make($data['password']);
            $user->fill($data);
            $id = $user->save();
            if ($id != 1) {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => 'Add user: Failed',
                );
                $ulogs->fill($data);
                $ulogs->save();

                Session::flash('error-message', 'Could not save user');
            } else {
                $user_data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => 'Add user: Successful',
                );
                $ulogs->fill($user_data);
                $ulogs->save();

                if (isset($data['permissions'])) {
                    foreach ($data['permissions'] as $permission) {
                        $usermodules = new UserModules();

                        $perm_data = array(
                            'user_id' => $user->id,
                            'module_id' => $permission,
                        );
                        $usermodules->fill($perm_data);
                        $usermodules->save();
                    }
                }
                Session::flash('success-message', 'User saved successfully');
            }
            return Redirect::to('add-user');
        }

        $view_data['contentdata']['modules'] = Modulesmodel::lists('name', 'id');
        $view_data['contentdata']['js'] = array("assets/js/system/common.js");
        $view_data['contentdata']['title'] = ' Add User';
        $view_data['content'] = 'users/add-user';
        return View::make('floor', $view_data);
    }

    public function allUsers() {
        $usersmodel = new User();
        $users = $usersmodel->all();

        $view_data['contentdata']['users'] = $users;
        $view_data['contentdata']['title'] = ' All Active Users';
        $view_data['content'] = 'users/all-users';
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        return View::make('floor', $view_data);
    }

    public function editUser($id) {
        $user_model = new User();
        $ulogs = new Userlog();
        $view_data['content'] = 'users/edit-user';
        $user_data = $user_model->find($id);
        $view_data['contentdata']['user'] = $user_data;
        $view_data['contentdata']['title'] = ' Edit Users';

        if (Request::method() == 'POST') {

            $data = Input::get();
            if (isset($data['permissions'])) {
                $data['modules'] = implode(',', $data['permissions']);
            }

            $user_data->fill($data);
            $update = $user_data->save();

            if ($update != 1) {

                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Edit User ID $id - Failed",
                );
                $ulogs->fill($data);
                $ulogs->save();

                Session::flash('error-message', 'Could not update user profile');
            } else {

                $user_data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Edit User ID $id - Successful ",
                );
                $ulogs->fill($user_data);
                $ulogs->save();

                if (isset($data['permissions'])) {
                    foreach ($data['permissions'] as $permission) {
                        $usermodules = new UserModules();

                        $perm_data = array(
                            'user_id' => $id,
                            'module_id' => $permission,
                        );
                        $usermodules->fill($perm_data);
                        $usermodules->save();
                    }
                }
                Session::flash('success-message', 'User profile updated');
            }
            return Redirect::to('users');
        }

        $view_data['contentdata']['js'] = array("assets/js/system/common.js");
        $view_data['contentdata']['modules'] = Modulesmodel::lists('name', 'id');

        return View::make('floor', $view_data);
    }

    function delete($id) {
        $user = User::find($id);
        $ulogs = new Userlog();
        $delete = false;

        if ($user) {
            $delete = $user->delete();

            if ($delete) {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Delete User ID $id - Successful",
                );
            } else {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Delete User ID $id - Failed",
                );
            }
        } else {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Delete User ID $id - Failed",
            );
        }
        $ulogs->fill($data);
        $ulogs->save();

        if (Request::ajax()) {
            echo $delete;
        } else {
            return $delete;
        }
    }

    public function deletedUsers() {
        $usersmodel = new User();
        $users = $usersmodel->onlyTrashed()->get();

        $view_data['contentdata']['users'] = $users;
        $view_data['contentdata']['title'] = ' Deleted Users';
        $view_data['contentdata']['type'] = 'deleted';
        $view_data['content'] = 'users/all-users';
        return View::make('floor', $view_data);
    }

    function restore($id) {
        $restore = User::withTrashed()->where('id', $id)->restore();
        $ulogs = new Userlog();
        if ($restore) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Restore deleted record User ID $id - Successful",
            );
        } else {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Restore deleted record User ID $id - Failed",
            );
        }

        $ulogs->fill($data);
        $ulogs->save();

        if (Request::ajax()) {
            echo $restore;
        } else {
            return $restore;
        }
    }

}
