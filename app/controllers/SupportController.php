<?php

class SupportController extends BaseController {

    public function save() {
        $data = Input::get();
        $data['user_id'] = isset(Auth::user()->id) ? Auth::user()->id : 0;
        $data['status'] = 1;
        $support = new Support();
        $support->fill($data);
        $save = $support->save();

        self::logs('Save Support Issue');

        $data['msg'] = $data['message'];
        \Mail::send('assets.supportemail', $data, function( $message ) use ($data) {
            $message->to('mailmukundi@gmail.com', 'Support Team')
                    ->subject('You have a ' . $data['subject'] . ' from ' . Auth::user()->name)
                    ->from('disposal@ams.com', 'AMS Disposal Team');
        });
    }

    function supportIssues() {
        $view_data['contentdata']['assets'] = DB::table('support')
                ->leftjoin('users', 'support.user_id', '=', 'users.id')
                ->leftjoin('support_status', 'support.status', '=', 'support_status.id')
                ->leftjoin('support_assignment', 'support.id', '=', 'support_assignment.support_id')
                ->select('support_assignment.user_id as assigned_to', 'users.*', 'support.*', 'support_status.name as status_label', 'support_status.class')
                ->whereNull('support.deleted_at')
                ->get();

        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/support.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'support/all-messages';
        $view_data['contentdata']['form-actions'] = '';
        $view_data['contentdata']['users'] = User::lists('name', 'id');

        self::logs('View Support Issues');
        return View::make('floor', $view_data);
    }

    function logs($action) {
        $ulogs = new Userlog();

        if (Auth::check()) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => $action,
            );
        } else {
            $data = array(
                'user_id' => 0,
                'user_email' => 0,
                'action' => $action,
            );
        }
        $ulogs->fill($data);
        $ulogs->save();
    }

}
