<?php

class DashController extends BaseController {

    public function index() {
        $assets = new Assetsmodel();

        $view_data['contentdata']['title'] = ' ';
        $view_data['contentdata']['caption'] = ' ';

        $assets = DB::table('assets')
                ->whereNull('assets.deleted_at')
                ->count();
        $users = DB::table('users')
                ->whereNull('deleted_at')
                ->count();
        $disp_assets = DB::table('asset-disposal')
                ->whereNull('deleted_at')
                ->count();

        $view_data['contentdata']['summary']['users'] = $users;
        $view_data['contentdata']['summary']['assets'] = $assets;
        $view_data['contentdata']['summary']['disp-assets'] = $disp_assets;
        
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'reports/dashboard';
        $view_data['contentdata']['form-actions'] = '';

        return View::make('floor', $view_data);
    }

}
