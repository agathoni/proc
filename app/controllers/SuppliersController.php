<?php

class SuppliersController extends BaseController {

    public function __construct() {
        $this->beforeFilter('haspermission:1');
    }

    public function addSupplier() {
        $ulogs = new Userlog();

        if (Request::method() == 'POST') {
            $data = Input::get();

            $user = new Suppliersmodel();
            $user->fill($data);
            $id = $user->save();
            if ($id != 1) {
                $action = "Could not add supplier";
            } else {
                $action = "Add supplier";
            }
            self::logs($action);
            echo $id;
        }
    }

    public function allSuppliers() {
        $usersmodel = new Suppliersmodel();
        $users = $usersmodel->all();

        $view_data['contentdata']['suppliers'] = $users;
        $view_data['contentdata']['title'] = ' All Active Suppliers';
        $view_data['content'] = 'suppliers/all-suppliers';
        $view_data['contentdata']['js'] = array("assets/js/system/suppliers.js", "assets/js/system/common.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');
        $view_data['contentdata']['form-actions'] = '<a data-toggle="modal" href="#add" class="btn btn-primary add-asset">Add Supplier</a>';

        return View::make('floor', $view_data);
    }

    public function editSupplier($id) {
        $ulogs = new Userlog();
        $suppliersmodel = new Suppliersmodel();
        $user_data = $suppliersmodel->find($id);
        if (Request::method() == 'POST') {

            $data = Input::get();
            $user_data->fill($data);
            $update = $user_data->save();

            if ($update != 1) {

                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Edit Supplier ID $id - Failed",
                );
                $ulogs->fill($data);
                $ulogs->save();
            } else {

                $user_data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Edit Supplier ID $id - Successful ",
                );
                $ulogs->fill($user_data);
                $ulogs->save();
            }
            echo json_encode($update);
        } else {

            echo json_encode($user_data);
        }
    }

    function delete($id) {
        $user = Suppliersmodel::find($id);
        $ulogs = new Userlog();
        $delete = false;

        if ($user) {
            $delete = $user->delete();
            if ($delete) {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Delete Supplier ID $id - Successful",
                );
            } else {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => "Delete Supplier ID $id - Failed",
                );
            }
        } else {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Delete Supplier ID $id - Failed",
            );
        }
        $ulogs->fill($data);
        $ulogs->save();

        if (Request::ajax()) {
            echo $delete;
        } else {
            return $delete;
        }
    }

    public function deletedSuppliers() {
        $usersmodel = new Suppliersmodel();
        $users = $usersmodel->onlyTrashed()->get();

        $view_data['contentdata']['suppliers'] = $users;
        $view_data['contentdata']['type'] = 'deleted';
        $view_data['content'] = 'suppliers/all-suppliers';
        $view_data['contentdata']['title'] = ' All Inactive Suppliers';
        $view_data['contentdata']['js'] = array("assets/js/system/suppliers.js", "assets/js/system/common.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');
        return View::make('floor', $view_data);
    }

    function restore($id) {
        $restore = Suppliersmodel::withTrashed()->where('id', $id)->restore();
        $ulogs = new Userlog();
        if ($restore) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Restore deleted  Supplier ID $id - Successful",
            );
        } else {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => "Restore deleted SupplierID $id - Failed",
            );
        }

        $ulogs->fill($data);
        $ulogs->save();

        if (Request::ajax()) {
            echo $restore;
        } else {
            return $restore;
        }
    }

    function logs($action) {
        $ulogs = new Userlog();

        if (Auth::check()) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => $action,
            );
        } else {
            $data = array(
                'user_id' => 0,
                'user_email' => 0,
                'action' => $action,
            );
        }
        $ulogs->fill($data);
        $ulogs->save();
    }

}
