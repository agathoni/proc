<?php

class categoriesController extends BaseController {

    public function __construct() {
        $this->beforeFilter('haspermission:2');
    }

    public function allCategories() {
        $categoriesmodel = new Categoriesmodel();

        $view_data['contentdata']['title'] = 'All Active Categories';
        $view_data['contentdata']['caption'] = 'List of Active Categories';
        $view_data['contentdata']['categories'] = $categoriesmodel->all();
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/categories.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assetcat/categories';
        $view_data['contentdata']['form-actions'] = '<a data-toggle="modal" href="#add-category" class="btn btn-primary add-category">Add category</a>';

        self::logs('View categories');
        return View::make('floor', $view_data);
    }

    public function addCategory() {

        if (Request::method() == 'POST') {
            $data = Input::get();

            $categories = new Categoriesmodel();
            $categories->fill($data);
            $id = $categories->save();

            if ($id != 1) {
                $action = 'Add Category: Failed';
                Session::flash('error-message', 'Could not save category');
            } else {
                $action = 'Add Category: Successful';
                Session::flash('success-message', 'Category saved successfully');
            }
            self::logs($action);
            if (Request::ajax()) {
                echo $id;
                exit;
            }
        }

        $view_data['contentdata']['title'] = ' Add User';
        $view_data['content'] = 'assetcat/add-category';

        if (Request::ajax()) {
            echo View::make($view_data['content'], $view_data);
            exit;
        } else {
            $view_data['contentdata']['title'] = ' Add Category';
            return View::make('floor', $view_data);
        }
    }

    public function editCategory($id) {

        if (Request::method() == 'POST') {
            $data = Input::get();

            $categories = new Categoriesmodel();
            $category = $categories->find($id);
            $category->fill($data);
            $id = $category->save();

            if ($id != 1) {
                $action = "Edit Category ID $id: Failed";
                Session::flash('error-message', 'Could not updated category');
            } else {
                $action = "Edit Category ID $id: Successful";
                Session::flash('success-message', 'Category updated successfully');
            }
            self::logs($action);
            if (Request::ajax()) {
                echo $id;
                exit;
            }
        }
    }

    public function deleteCategory($id) {

        $categories = new Categoriesmodel();
        $category = $categories->find($id);

        $update = 0;
        if ($category) {
            $update = $category->delete();

            if ($update != 1) {
                $action = "Delete Category ID $id: Failed";
                Session::flash('error-message', 'Could not updated category');
            } else {
                $action = "Delete Category ID $id: Successful";
                Session::flash('success-message', 'Category updated successfully');
            }
        } else {
            $action = "Delete Category ID $id: Failed";
            Session::flash('error-message', 'Could not updated category');
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $update;
            exit;
        } else {
            return $update;
        }
    }

    public function restoreCategory($id) {

        $restore = Categoriesmodel::withTrashed()->where('id', $id)->restore();

        if ($restore) {
            $action = "Restore Category ID $id: Failed";
            Session::flash('error-message', 'Could not restore category');
        } else {
            $action = "Restore Category ID $id: Failed";
            Session::flash('error-message', 'Could not restore category');
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $restore;
            exit;
        }
    }

    public function deletedcategories() {
        $categoriesmodel = new Categoriesmodel();

        $view_data['contentdata']['title'] = 'Deleted Categories';
        $view_data['contentdata']['caption'] = 'List of inactive Categories';
        $view_data['contentdata']['categories'] = $categoriesmodel->onlyTrashed()->get();
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/categories.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');
        $view_data['contentdata']['type'] = 'deleted';
        $view_data['content'] = 'assetcat/categories';
        return View::make('floor', $view_data);
    }

    function logs($action) {
        $ulogs = new Userlog();

        if (Auth::check()) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => $action,
            );
        } else {
            $data = array(
                'user_id' => 0,
                'user_email' => 0,
                'action' => $action,
            );
        }
        $ulogs->fill($data);
        $ulogs->save();
    }

}
