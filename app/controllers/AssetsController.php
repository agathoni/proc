<?php

class AssetsController extends BaseController {

    public function __construct() {
        $this->beforeFilter('haspermission:3');
    }

    public function all() {
        $assets = new Assetsmodel();

        $view_data['contentdata']['title'] = 'All Active Assets';
        $view_data['contentdata']['caption'] = 'List of Active Assets';
        $view_data['contentdata']['assets'] = DB::table('assets')
                ->leftjoin('asset_categories', 'assets.category_id', '=', 'asset_categories.id')
                ->leftjoin('asset_status', 'assets.status', '=', 'asset_status.id')
                ->leftjoin('asset-assignment', 'assets.id', '=', 'asset-assignment.asset_id')
                ->select('asset-assignment.id as assignmentid', 'assets.image', 'assets.assigned', 'assets.id', 'assets.amount', 'assets.name', 'assets.note', 'asset_categories.name as category', 'asset_status.name as status')
                ->whereNull('assets.deleted_at')
                ->whereNull('assets.disposed')
                ->get();

        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assets/all-assets';
        $view_data['contentdata']['form-actions'] = '<a data-toggle="modal" href="#add" class="btn btn-primary add-asset">Add Asset</a>';

        $view_data['contentdata']['users'] = User::lists('name', 'id');

        self::logs('View Assets');
        return View::make('floor', $view_data);
    }

    public function add() {

        if (Request::method() == 'POST') {
            $data = Input::get();
            $data['received_by'] = isset(Auth::user()->id) ? Auth::user()->id : 0;

            if (Input::hasFile('image')) {
                $file = Input::file('image')->getClientOriginalName();
                $extension = Input::file('image')->getClientOriginalExtension();

                $destinationPath = 'product_images/large';
                $filename = Str::random(20) . '.' . $extension;

                $data['image'] = $filename;
                $upload_success = Input::file('image')
                        ->move($destinationPath, $filename);
            }
            $bulkassets = new AssetsReceivedmodel();
            $bulkassets->fill($data);
            $bulkassets->save();

            for ($i = 0; $i < $data['units']; $i++) {
                $data['ar_id'] = $bulkassets->id;
                $assetsmodel = new Assetsmodel();
                $assetsmodel->fill($data);
                $save = $assetsmodel->save();
            }

            if ($save != 1) {
                $action = 'Add Asset: Failed';
                Session::flash('error-message', 'Could not save Asset');
            } else {
                $action = 'Add Asset: Successful';
                Session::flash('success-message', 'Asset saved successfully');
            }
            self::logs($action);
            if (Request::ajax()) {
                $response[] = $assetsmodel->id;
                $response[] = isset($filename) ? $filename : '';
                echo json_encode($response);
                exit;
            }
        }

        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js");

        $view_data['contentdata']['title'] = ' Add Asset';
        $view_data['content'] = 'assets/add-asset';

        if (Request::ajax()) {
            $view_data['suppliers'] = Suppliersmodel::lists('name', 'id');
            $view_data['categories'] = Categoriesmodel::lists('name', 'id');
            $view_data['status'] = Assetstatusmodel::lists('name', 'id');
            echo View::make($view_data['content'], $view_data);
            exit;
        } else {
            $view_data['contentdata']['suppliers'] = Suppliersmodel::lists('name', 'id');
            $view_data['contentdata']['categories'] = Categoriesmodel::lists('name', 'id');
            $view_data['contentdata']['status'] = Assetstatusmodel::lists('name', 'id');
            return View::make('floor', $view_data);
        }
    }

    public function edit($id) {
        $categories = new Assetsmodel();
        $category = $categories->find($id);
        if (Request::method() == 'POST') {
            $data = Input::get();
            if (Input::hasFile('image')) {
                $file = Input::file('image')->getClientOriginalName();
                $extension = Input::file('image')->getClientOriginalExtension();
                $destinationPath = 'product_images/large';
                $filename = Str::random(20) . '.' . $extension;
                $data['image'] = $filename;
                $upload_success = Input::file('image')
                        ->move($destinationPath, $filename);
            } else {
                $data['image'] = $category->image;
            }

            $category->fill($data);
            $id = $category->save();

            if ($id != 1) {
                $action = "Edit Asset ID $id: Failed";
                Session::flash('error-message', 'Could not updated Asset');
            } else {
                $action = "Edit Asset ID $id: Successful";
                Session::flash('success-message', 'Asset updated successfully');
            }
            self::logs($action);
            if (Request::ajax()) {
                $response[] = $id;
                $response[] = isset($filename) ? $filename : $category->image;
                echo json_encode($response);
                exit;
            }
        } else {
            $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js");
            $view_data['contentdata']['title'] = ' Edit Asset';
            $view_data['content'] = 'assets/add-asset';

            if (Request::ajax()) {
                $view_data['categories'] = Categoriesmodel::lists('name', 'id');
                $view_data['status'] = Assetstatusmodel::lists('name', 'id');
                $view_data['suppliers'] = Suppliersmodel::lists('name', 'id');

                echo View::make($view_data['content'], $view_data);
                exit;
            } else {
                echo 'test';
                return;
            }
        }
    }

    public function data($id) {
        $category = DB::table('assets')
                ->leftjoin('assets_received', 'assets.ar_id', '=', 'assets_received.id')
                ->select('assets.*', 'assets_received.supplier_id', 'assets_received.units', 'assets_received.bulkprice')
                ->where("assets.id", "=", $id);
        if (Request::ajax()) {
            echo json_encode($category->get());
            exit;
        } else {
            echo '<pre>';
            print_r($category->get());
            echo '</pre>';
        }
    }

    public function delete($id) {

        $categories = new Assetsmodel();
        $category = $categories->find($id);

        $update = 0;
        if ($category) {
            $update = $category->delete();

            if ($update != 1) {
                $action = "Delete Asset ID $id: Failed";
                Session::flash('error-message', 'Could not updated Asset');
            } else {
                $action = "Delete Asset ID $id: Successful";
                Session::flash('success-message', 'Asset updated successfully');
            }
        } else {
            $action = "Delete Asset ID $id: Failed";
            Session::flash('error-message', 'Could not updated Asset');
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $update;
            exit;
        } else {
            return $update;
        }
    }

    public function restore($id) {

        $restore = Assetsmodel::withTrashed()->where('id', $id)->restore();

        if ($restore) {
            $action = "Restore Asset ID $id: Failed";
            Session::flash('error-message', 'Could not restore Asset');
        } else {
            $action = "Restore Asset ID $id: Failed";
            Session::flash('error-message', 'Could not restore Asset');
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $restore;
            exit;
        }
    }

    public function deleted() {
        $assets = new Assetsmodel();

        $view_data['contentdata']['title'] = 'All Inactive Assets';
        $view_data['contentdata']['caption'] = 'List of Inactive Assets';
        $view_data['contentdata']['assets'] = $assets->onlyTrashed()->get();

        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assets/deleted-assets';
        $view_data['contentdata']['form-actions'] = '<a data-toggle="modal" href="#add" class="btn btn-primary add-asset">Add Asset</a>';
        $view_data['contentdata']['type'] = 'deleted';

        return View::make('floor', $view_data);
    }

    function logs($action) {
        $ulogs = new Userlog();

        if (Auth::check()) {
            $data = array(
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->email,
                'action' => $action,
            );
        } else {
            $data = array(
                'user_id' => 0,
                'user_email' => 0,
                'action' => $action,
            );
        }
        $ulogs->fill($data);
        $ulogs->save();
    }

    public function dispose($id) {
        $data = Input::get();
        $data['status'] = 1;
        $data['asset_id'] = $id;
        $assetsmodel = new AssetDisposal();
        $assetsmodel->fill($data);
        $save = $assetsmodel->save();

        //update asset status
        $categories = new Assetsmodel();
        $category = $categories->find($id);
        $assetdata['disposed'] = 1;
        $category->fill($assetdata);
        $id = $category->save();

        if ($save != 1) {
            $action = 'Dispose Asset: Failed';
            Session::flash('error-message', 'Could not dispose Asset');
        } else {
            $action = 'Dispose Asset: Successful';
            Session::flash('success-message', 'Asset disposed successfully');
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $save;
            exit;
        }
    }

    public function disposed() {
        $assets = new Assetsmodel();

        $view_data['contentdata']['title'] = 'All disposable Assets';
        $view_data['contentdata']['caption'] = 'List of Inactive Assets';
        $view_data['contentdata']['assets'] = DB::table('asset-disposal')
                ->leftjoin('assets', 'asset-disposal.asset_id', '=', 'assets.id')
                ->leftjoin('asset_categories', 'assets.category_id', '=', 'asset_categories.id')
                ->leftjoin('asset_status', 'assets.status', '=', 'asset_status.id')
                ->leftjoin('disposal_details', 'asset-disposal.asset_id', '=', 'disposal_details.disposed_asset_id')
                ->select('disposal_details.names','disposal_details.email','disposal_details.phone_no','asset-disposal.disposemethod', 'asset-disposal.amount as disposalamount', 'asset-disposal.note', 'asset-disposal.status as disposalstatus', 'asset-disposal.id', 'assets.amount', 'assets.name', 'asset_categories.name as category', 'asset_status.name as status')
                ->whereNull('asset-disposal.deleted_at')
//                ->where('disposed', '!=', 0)
                ->get();
        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assets/disposed-assets';
        $view_data['contentdata']['form-actions'] = '';
        $view_data['contentdata']['type'] = 'deleted';

        return View::make('floor', $view_data);
    }

    public function deletedisposable($id) {

        $categories = new AssetDisposal();
        $category = $categories->find($id);

        $update = 0;
        if ($category) {
            $update = $category->delete();

            if ($update != 1) {
                $action = "Restore Disposable item ID $id: Failed";
            } else {
                $action = "Restore Disposable item  ID $id: Successful";
            }
        } else {
            $action = "Restore Disposable item  ID $id: Failed";
        }

        //update asset status
        $categories = new Assetsmodel();
        $category = $categories->find($id);
        $assetdata['disposed'] = null;
        $category->fill($assetdata);
        $id = $category->save();

        self::logs($action);
        if (Request::ajax()) {
            echo $update;
            exit;
        } else {
            return $update;
        }
    }

    public function editDisposed($id) {
        $categories = new AssetDisposal();
        $category = $categories->find($id);

//        if ($category->disposemethod == 2) {
//            
//
//            return View::make('assets/email', $view_data);
//            exit;
//            $message = array(
//                'subject' => 'Testing mail',
//                'from_email' => 'mailmukundi@gmail.com',
//                'from_name' => 'mailmukundi@gmail.com',
//                'to' => array(array(
//                        'email' => 'mailmukundi@gmail.com',
//                        'name' => 'Assset Management team'
//                    )),
//                'merge_vars' => array(array(
//                        'rcpt' => 'mailmukundi@gmail.com',
//                        'vars' => array(
//                            array(
//                                'name' => 'NAME',
//                                'content' => 'Mzoori'
//                            ),
//                            array(
//                                'name' => 'MESSAGE',
//                                'content' => 'Some message'
//                            )
//                        )
//                    ))
//            );
//            $template_content = array(
//                array(
//                    'name' => 'main',
//                    'content' => '*|NAME|*,*|MESSAGE|*')
//            );
//
//
//            $msg = \Swift_Message::newInstance()
//                    ->setSubject('Subject')
//                    ->setFrom('test@umpirss.com')
//                    ->setTo('mailmukundi@gmail.com')
//                    ->setBody('testwwww');
//
//            $sent = Mail::send('hello', [], function($message) {
//                        $message->to('mailmukundi@gmail.com', 'Ansel Melly')
//                                ->from('mailmukundi@gmail.com', 'Laravel')
//                                ->subject('Hello World 111');
//                    });
//
//            Mail::send('hello', array('key' => 'value'), function($message) {
//                $message->to('mailmukundi@gmail.com', 'John Smith')
//                        ->subject('Welcome!')
//                        ->from('mailmukundi@gmail.com', 'Laravel');
//            });
//
//            echo '<pre>';
////            print_r($msg);
//            echo '</pre>';
//            exit;
//
//            $msg = \Swift_Message::newInstance();
//            $msg->template = "proc-order";
//            $msg->content = array($template_content);
//            $msg->message = $message;
//
//
////            Mail::send('common.header', [], function($message) {
////                $message->to('mailmukundi@gmail.com', 'Ansel Melly')
////                        ->subject('Hello World');
////            });
//            self::send($msg);
////            exit;
////            
////            $this->send($message);
////            $this->get('mailer')->send($message);
////            exit;
////            $this->messages->send('proc-order', $template_content, $message);
////            $msg = new \Monolog\Handler\SwiftMailerHandler($mailer, $message);
////            $mandrill = new \Illuminate\Mail\Transport\MandrillTransport('Yhhu1M-45DC5_cWDnZ1YQA');
////            $mandrill->send($message);
////            $response = $mandrill->messages->sendTemplate('proc-order', $template_content, $message);
//        }
//        echo $category->disposemethod;
//        exit;
        if (Request::method() == 'POST') {
            $data = Input::get();
            $category->fill(array('status' => $data['status']));
            $id = $category->save();

            $disposal_details = new DisposalDetails();
            $data['disposed_asset_id'] = $id;
            $disposal_details->fill($data);
            $disposal_details->save($data);

            $asstsmodel = new Assetsmodel();
            $view_data['data'] = $data;
            $view_data['disposal'] = $category;
            $view_data['asset'] = $asstsmodel->find($category->asset_id);

            if ($category->disposemethod == 2) {
                $view_data['title'] = 'AMS ORDER ';
            } elseif ($category->disposemethod == 0) {
                $view_data['title'] = 'AMS DONATION ';
            } else {
                $view_data['title'] = 'AMS DISPOSAL ';
            }

            $data['title'] = $view_data['title'];
            \Mail::send('assets.email', $view_data, function( $message ) use ($data) {
                $message->to($data['email'], $data['names'])
                        ->subject($data['title'])
                        ->from('disposal@ams.com', 'AMS Disposal Team');
            });

            if ($id != 1) {
                $action = "Edit disposed item ID $id: Failed";
            } else {
                $action = "Edit disposed item ID $id: Successful";
            }
            self::logs($action);
            if (Request::ajax()) {
                echo $id;
                exit;
            }
        }
    }

    public function send(Swift_Mime_Message $message, &$failedRecipients = null) {
        $mandrill = new \Illuminate\Mail\Transport\MandrillTransport('Yhhu1M-45DC5_cWDnZ1YQA');
        $send = $mandrill->send($message);
        echo 'testonnnn';
        print_r($send);
        exit;
    }

    public function assign($id) {
        $data = Input::get();
        $data['status'] = 1;
        $data['asset_id'] = $id;
        $assetsmodel = new Assetsassignmentmodel();
        $assetsmodel->fill($data);
        $save = $assetsmodel->save();

        //update asset status
        $categories = new Assetsmodel();
        $category = $categories->find($id);
        $assetdata['assigned'] = 1;
        $category->fill($assetdata);
        $id = $category->save();

        if ($save != 1) {
            $action = 'Assign Asset: Failed';
        } else {
            $action = 'Assign Asset: Successful';
        }

        self::logs($action);
        if (Request::ajax()) {
            echo $save;
            exit;
        }
    }

    public function editAssign($id) {
        $categories = new Assetsassignmentmodel();
        $category = $categories->find($id);
        $data = Input::get();
        $category->fill($data);
        $id = $category->save();

        if ($id != 1) {
            $action = "Edit asset  assignment  ID $id: Failed";
        } else {
            $action = "Edit asset assignment  ID $id: Successful";
        }
        self::logs($action);
        if (Request::ajax()) {
            echo $id;
            exit;
        }
    }

    public function receivebulk() {
        if (Request::method() == 'POST') {
            $data = Input::get();
            $data['received_by'] = isset(Auth::user()->id) ? Auth::user()->id : 0;
            $assetsmodel = new Assetsmodel();
            $assetsmodel->fill($data);
            $save = $assetsmodel->save();

            if ($save != 1) {
                $action = 'Add Asset: Failed';
                Session::flash('error-message', 'Could not save Asset');
            } else {
                $action = 'Add Category: Successful';
                Session::flash('success-message', 'Asset saved successfully');
            }
            self::logs($action);
            if (Request::ajax()) {
                echo $assetsmodel->id;
                exit;
            }
        }
    }

    function supplies() {
        $view_data['contentdata']['title'] = 'Bulk supplies';
        $view_data['contentdata']['caption'] = ' ';
        $view_data['contentdata']['assets'] = DB::table('assets_received')
                ->leftjoin('asset_categories', 'assets_received.category_id', '=', 'asset_categories.id')
                ->leftjoin('asset_status', 'assets_received.status', '=', 'asset_status.id')
                ->leftjoin('users', 'assets_received.received_by', '=', 'users.id')
                ->leftjoin('suppliers', 'assets_received.supplier_id', '=', 'suppliers.id')
                ->select('users.name as received_by', 'assets_received.*', 'asset_categories.name as category', 'suppliers.name as supplier_name', 'suppliers.created_at')
                ->get();


        $view_data['contentdata']['js'] = array("assets/js/system/common.js", "assets/js/system/assets.js", "assets/plugins/datatables/jquery.dataTables.min.js", "assets/plugins/datatables/dataTables.bootstrap.js", "assets/demo/demo-datatables.js");
        $view_data['contentdata']['css'] = array('assets/plugins/datatables/dataTables.css');

        $view_data['content'] = 'assets/supplies';
//        $view_data['contentdata']['form-actions'] = '<a data-toggle="modal" href="#add" class="btn btn-primary add-asset">Add Asset</a>';

        $view_data['contentdata']['users'] = User::lists('name', 'id');

        self::logs('View Supplies');
        return View::make('floor', $view_data);
    }

    function emailtest($assetid) {
        $view_data['asset'] = Assetsmodel::find($assetid)->get();
        echo '<pre>';
        print_r($view_data['asset']->image);
        echo '</pre>';
        exit;
        return View::make('assets/email', $view_data);
    }

}
