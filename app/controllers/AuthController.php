<?php

class AuthController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function login() {

        if (Request::method() == 'POST') {
            $data = Input::get();
            $ulogs = new Userlog();
            if (Auth::attempt(array('email' => $data['email'], 'password' => $data['password']))) {
                $data = array(
                    'user_id' => Auth::user()->id,
                    'user_email' => Auth::user()->email,
                    'action' => 'Login Successful',
                );
                $ulogs->fill($data);
                $ulogs->save();

                return Redirect::intended('');
            } else {
                $data = array(
                    'user_email' => $data['email'],
                    'action' => 'Login failed',
                );
                $ulogs->fill($data);
                $ulogs->save();

                return Redirect::to('login');
            }
//            echo ' testing login';
//            exit;
        }

        return View::make('users/login');
    }

}
