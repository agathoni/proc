<?php echo View::make('common/header-focused'); ?>


<div class="verticalcenter">
    <?php if (Session::has('error-message')) { ?>
        <div class="alert alert-dismissable alert-danger">
            <strong>Oh snap!</strong> <?php echo Session::get('error-message') ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
    <?php }; ?>
    <?php if (Session::has('success-message')) { ?>
        <div class="alert alert-dismissable alert-success">
            <strong>Well done!</strong> <?php echo Session::get('success-message') ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
    <?php }; ?>

    <a href="index.php">
        <!--<img src="assets/img/logo-big.png" alt="Logo" class="brand" />-->
    </a>
    <br/>
    <div class="panel panel-primary">
        <div class="panel-body">
            <h4 class="text-center" style="margin-bottom: 25px;">Log in to get started </h4>

            <form action="<?php echo url(); ?>/login" class="form-horizontal" style="margin-bottom: 0px !important;" method="POST">

                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="email"  id="email" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password"  id="password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                        <!--<div class="pull-right"><label><input type="checkbox" style="margin-bottom: 20px" checked=""> Remember Me</label></div>-->
                </div>

                <div class="panel-footer">
                    <!--<a href="extras-forgotpassword.php" class="pull-left btn btn-link" style="padding-left:0">Forgot password?</a>-->

                    <div class="pull-right">
                        <!--<a href="#" class="btn btn-default">Reset</a>-->
                        <button class="btn btn-primary">Log In</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
</div>

</body>
</html>