<div class="panel-body collapse in">
    <?php
    $settings = array(
        'class' => 'form-horizontal row-border',
        'method' => 'POST',
//                'route' => 'edit-user', $user->id,
    );
    echo Form::model('user', $settings);
    ?>
    <div class="form-group">
        <?php echo Form::label('name', 'Names:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('name', $value = $user->name, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('email', 'Email:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::email('email', $value = $user->email, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('phone', 'Phone:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('phone', $value = $user->phone, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('Permissions', 'Permissions:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php
            $my_modules = explode(',', Auth::user()->modules);
            foreach ($modules as $key => $module) {
                $checked = '';
                if (in_array($key, $my_modules)) {
                    $checked = 'checked';
                }
                ?>
                <input <?php echo $checked ?> class="permissions" name="permissions[]" type="checkbox" value="<?php echo $key ?>"> <?php echo $module ?>
            <?php } ?>

        </div>
    </div>
    <br/>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <button class="btn-primary btn save-form">Submit</button>
                    <button class="btn-default btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>
</div>
