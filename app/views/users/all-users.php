<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <!--<th style="padding-right:100px">Registration Date</th>-->
                    <th>ID</th>
                    <th>Names</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($users as $user) {
                    ?>
                    <tr>
                        <!--<td align="left"><i>World</i></td>-->
                        <td><?php echo $user->id ?></td>
                        <td><?php echo ucwords(strtolower($user->name)) ?></td>
                        <td><?php echo $user->email ?></td>
                        <td><?php echo $user->phone ?></td>
                        <td>
                            <?php if (!isset($type) || $type !== 'deleted') { ?> 
                                <a href="edit-user/<?php echo $user->id ?>" class="btn btn-primary btn-xs edit-user" id="edit-<?php echo $user->id ?>">
                                    Edit User
                                </a>    
                                <!--<hr>-->
                                <a href="users/user-logs/<?php echo $user->id ?>" class="btn btn-primary btn-xs edit-user" id="edit-<?php echo $user->id ?>">
                                    User Logs
                                </a>   
                                <a href="javascript:;" class="btn btn-danger btn-xs delete-user" id="delete-<?php echo $user->id ?>">
                                    Delete User
                                </a>
                            <?php } else {
                                ?>
                                <a href="javascript:;" class="btn btn-green btn-xs restore-user" id="restore-<?php echo $user->id ?>">
                                    Restore User
                                </a>
                                <?php }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <caption>List of system users </caption>
        </table>
    </div>
</div>