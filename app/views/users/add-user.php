<div class="panel-body collapse in">
    <?php echo Form::model('user', array('class' => 'form-horizontal row-border user-form', 'method' => 'post')); ?>
    <div class="form-group">
        <?php echo Form::label('name', 'Names:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('name', $value = null, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('email', 'Email:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::email('email', $value = null, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('phone', 'Phone:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('phone', $value = null, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('password', 'Password:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::password('password', $value = null, $attributes = array('class' => 'form-control','required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('Permissions', 'Permissions:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php foreach ($modules as $key => $module) { ?>
                <input required class="permissions" name="permissions[]" type="checkbox" value="<?php echo $key ?>"> <?php echo $module ?>
            <?php } ?>
                
        </div>
    </div>
    <br/>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <button class="btn-primary btn save-user save-form">Submit</button>
                    <button class="btn-default btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
<?php echo Form::close(); ?>

</div>
