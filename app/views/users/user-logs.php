<div class="panel-body collapse in">
    <!--<div class="table-responsive">-->
    <table class="table table-striped table-bordered datatables dataTable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>User ID</th>
                <th>Telephone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!isset($user)) {
                $logs = Userlog::all();
            } else {
                $logs = Userlog::where('user_id', '=', $user)->get();
            }

            foreach ($logs as $datum) {
                ?>
                <tr>
                    <td><?php echo $datum->id ?></td>
                    <td><?php echo $datum->created_at ?></td>
                    <td><?php echo $datum->name ?></td>
                    <td><?php echo $datum->user_email ?></td>
                    <td><?php echo $datum->action ?></td>
                </tr>
            <?php } ?>
        </tbody>
        <caption>List of system users actions </caption>
    </table>
    <!--</div>-->
</div>