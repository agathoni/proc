<?php
$contentdata['cdata'] = isset($contentdata) ? $contentdata : array();
echo View::make('common/header', $contentdata);
?>
<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <h1><?php echo isset($heading) ? $heading : '' ?></h1>
        </div>
        <div class="container">

            <?php if (Session::has('error-message')) { ?>
                <div class="alert alert-dismissable alert-danger">
                    <strong>Oh snap!</strong> <?php echo Session::get('error-message') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
            <?php }; ?>
            <?php if (Session::has('success-message')) { ?>
                <div class="alert alert-dismissable alert-success">
                    <strong>Well done!</strong> <?php echo Session::get('success-message') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
            <?php }; ?>
            <div class="row">
                <div class="panel panel-midnightblue">
                    <div class="panel-heading">
                        <h4><?php echo isset($contentdata['title']) ? $contentdata['title'] : ''; ?> </h4>

                        <div style="float:right">
                            <?php echo isset($contentdata['form-actions']) ? $contentdata['form-actions'] : ''; ?>
                        </div>
                    </div>
                </div>
                <?php
                echo View::make($content, $contentdata);
                ?>
            </div>
        </div>
    </div>
</div>
<?php echo View::make('common/footer', $contentdata); ?>
