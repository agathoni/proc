<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php
                if (count($suppliers) > 0) {
                    foreach ($suppliers as $asset) {
                        ?>
                        <tr class="tr-<?php echo $asset->id ?>">
                            <td><?php echo $asset->id ?>
                            </td>
                            <td class="center name-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->name)) ?></td>
                            <td class="center value-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->email)) ?></td>
                            <td class="center category-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->phone)) ?></td>
                            <td>
                                <?php if (!isset($type) || $type !== 'deleted') { ?> 
                                    <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs edit-asset" id="edit-<?php echo $asset->id ?>" >
                                        Edit Supplier
                                    </a>    
                                    <a href="javascript:;" class="btn btn-danger btn-xs delete-asset" id="delete-<?php echo $asset->id ?>">
                                        Delete Supplier
                                    </a>
                                    <?php }else {
                                    ?>
                                    <a href="javascript:;" class="btn btn-green btn-xs restore-asset" id="restore-<?php echo $asset->id ?>">
                                        Restore Supplier
                                    </a>
                                <?php }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade modals" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Supplier</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<span class='s-add-form' style='display:none'>

    <div class="panel-body collapse in">
            <?php echo Form::model('Suppliersmodel', array('class' => 'form form-horizontal row-border supplier-form', 'method' => 'post')); ?>
        <div class="form-group">
                <?php echo Form::label('name', 'Names:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
<?php echo Form::text('name', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
            </div>
        </div>
        <div class="form-group">
                <?php echo Form::label('email', 'Email:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
<?php echo Form::email('email', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
            </div>
        </div>
        <div class="form-group">
                <?php echo Form::label('phone', 'Phone:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
<?php echo Form::text('phone', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
            </div>
        </div>
        <br/>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-supplier-form ">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
<?php echo Form::close(); ?>

    </div>
</span>
