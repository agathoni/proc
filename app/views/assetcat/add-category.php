<div class="panel-body collapse in">
    <?php echo Form::model('Categoriesmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
    <div class="form-group">
        <?php echo Form::label('name', 'Name:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('name', $value = null, $attributes = array('class' => 'form-control')); ?>
        </div>
    </div>
    <br/>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <button class="btn-primary btn">Submit</button>
                    <a class="btn-default btn btn-cancel">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
