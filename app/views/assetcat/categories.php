<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Names</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($categories as $category) {
                    ?>
                    <tr class="tr-<?php echo $category->id ?>">
                        <td><?php echo $category->id ?></td>
                        <td class="cat-name-<?php echo $category->id ?>" ><?php echo ucwords(strtolower($category->name)) ?></td>
                        <td>
                            <?php if (!isset($type) || $type !== 'deleted') { ?> 
                                <a data-toggle="modal" href="#add-category" class="btn btn-primary btn-xs edit-category" id="edit-<?php echo $category->id ?>">
                                    Edit Category
                                </a>    
                                <a href="javascript:;" class="btn btn-danger btn-xs delete-category" id="delete-<?php echo $category->id ?>">
                                    Delete Category
                                </a>
                            <?php } else {
                                ?>
                                <a href="javascript:;" class="btn btn-green btn-xs restore-category" id="restore-<?php echo $category->id ?>">
                                    Restore Category
                                </a>
                            <?php }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <caption><?php echo isset($caption) ? $caption : '' ?> </caption>
        </table>
    </div>
</div>
<div class="modal fade modals" id="add-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Category</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Name:</label>        <div class="col-sm-6">
                        <input class="form-control" name="name" type="text" id="name" required>        </div>
                </div>
                <br style="border-radius: 0px;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-category">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>