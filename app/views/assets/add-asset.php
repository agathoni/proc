<div class="panel-body collapse in">
    <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border add-asset-form','enctype' => 'multipart/form-data', 'method' => 'post')); ?>
    <div class="form-group">
        <?php echo Form::label('name', 'Product Name:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::text('name', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('supplier', 'Supplier:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::select('supplier_id', $suppliers, null, $attributes = array('class' => 'form-control', 'required' => 'required', 'id' => 'supplier_id')) ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('units', 'No of units:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::number('units', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('amount', 'Price per unit:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::number('amount', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('bulkprice', 'Cumulative Price:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::number('bulkprice', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('name', 'Category:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::select('category_id', $categories, null, $attributes = array('class' => 'form-control', 'required' => 'required', 'id' => 'category_id')) ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('name', 'Status:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::select('status', $status, null, $attributes = array('class' => 'form-control', 'required' => 'required', 'id' => 'status')) ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('name', 'Description:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::textarea('note', $value = null, $attributes = array('class' => 'form-control', 'id' => 'note')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Form::label('image', 'Image:', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo Form::file('image', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <br/>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <button class="btn-primary btn save-asset">Submit</button>
                    <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
