<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php foreach ($assets as $asset) { ?>
                    <tr class="tr-<?php echo $asset->id ?>">
                        <td><?php echo $asset->id ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->name)) ?>
                            <hr>
                            <img width="100px" src="<?php echo url('product_images/large/' . ($asset->image)) ?>">
                        </td>
                        <td class="center value-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->amount)) ?></td>
                        <td class="center category-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->category)) ?></td>
                        <td class="center status-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->status)) ?>
                            <hr>
                            <?php echo ucwords(strtolower($asset->note)) ?>
                        </td>
                        <td>
                            <?php if (!isset($type) || $type !== 'deleted') { ?> 
                                <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs edit-asset" id="edit-<?php echo $asset->id ?>" >
                                    Edit Asset
                                </a>    
                                <a href="javascript:;" class="btn btn-danger btn-xs delete-asset" id="delete-<?php echo $asset->id ?>">
                                    Delete Asset
                                </a>
                                <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs dispose-asset" id="dispose-<?php echo $asset->id ?>" data-price="<?php echo $asset->amount ?>">
                                    Dispose
                                </a>  

                                <?php
                                if ($asset->assigned == 1) {
                                    ?>
                                    <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs reassign-asset" data-id="<?php echo $asset->assignmentid ?>" id="reassign-<?php echo $asset->id ?>" >
                                        Reassign
                                        <?php
                                    } else {
                                        ?>
                                        <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs assign-asset" data-id="<?php echo $asset->id ?>" id="assign-<?php echo $asset->id ?>" >
                                            Assign
                                        <?php } ?>
                                    </a>
                                <?php } else {
                                    ?>
                                    <a href="javascript:;" class="btn btn-green btn-xs restore-asset" id="restore-<?php echo $asset->id ?>">
                                        Restore Asset
                                    </a>
                                <?php }
                                ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade modals" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Asset</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<span class='disposal' style='display:none'>
    <div class="panel-body collapse in">
        <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
        <div class="form-group">
            <?php echo Form::label('disposemethod', 'Dispose method:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::select('disposemethod', array('donate', 'throw away', 'sell'), null, $attributes = array('class' => 'form-control', 'required' => 'required')) ?>
            </div>
        </div>
        <div class="form-group cost">
            <?php echo Form::label('amount', 'Amount:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::number('amount', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'Cost that will be incurred')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('name', 'Note:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::textarea('note', $value = null, $attributes = array('class' => 'form-control', 'id' => 'note')); ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-dispose-asset">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</span>

<span class='assign-form' style='display:none'>
    <div class="panel-body collapse in">
        <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
        <div class="form-group">
            <?php echo Form::label('user', 'User:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php
                echo Form::select('assignuser_id', $users, null, $attributes = array('class' => 'form-control', 'required' => 'required', 'id' => 'assignuser_id'))
                ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-assign-asset">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</span>