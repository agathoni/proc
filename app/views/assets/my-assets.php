<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php foreach ($assets as $asset) { ?>
                    <tr class="tr-<?php echo $asset->id ?>">
                        <td><?php echo $asset->id ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->name)) ?>
                            <hr>
                            <img width="100px" src="<?php echo url('product_images/large/' . ($asset->image)) ?>">
                        </td>
                        <td class="center value-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->amount)) ?></td>
                        <td class="center category-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->category)) ?></td>
                        <td class="center status-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->status)) ?>
                            <hr>
                            <?php echo ucwords(strtolower($asset->note)) ?>
                        </td>
                        <td>
                            <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs create-support" data-id="<?php echo $asset->id ?>">
                                Report problem / Make suggestion
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade modals" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create ticket</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<span class='support-form' style='display:none'>
    <div class="panel-body collapse in">
        <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
        <div class="form-group">
            <?php echo Form::label('Subject', 'Subject:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <select id="subject" class="form-control  " required="" name="subject">
                    <option value="">--SELECT--</option>
                    <option value="Problem"> Problem </option>
                    <option value="Complaint"> Complaint </option>
                    <option value="Question"> Question </option>
                    <option value="Idea/Suggestion"> Idea/Suggestion </option>
                    <option value="Compliment"> Compliment </option>
                    <option value="Product/Service Request"> Product/Service Request </option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('message', 'Message:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::textarea('message', $value = null, $attributes = array('class' => 'form-control', 'id' => 'note')); ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-support-issue">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</span>

 