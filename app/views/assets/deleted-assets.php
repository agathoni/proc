<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php
                foreach ($assets as $asset) {
                    ?>
                    <tr class="tr-<?php echo $asset->id ?>">
                        <td><?php echo $asset->id ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->name))  ?></td>
                        <td class="center value-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->amount))  ?></td>
                        <td class="center status-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->status))  ?></td>
                        <td>
                            <a href="javascript:;" class="btn btn-green btn-xs restore-asset" id="restore-<?php echo $asset->id ?>">
                                Restore Asset
                            </a>
                        </td>
                    </tr>
<?php } ?>
            </tbody>
        </table>
    </div>
</div>
