<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Product</th>
                    <th>Units</th>
                    <th>Value</th>
                    <th>Details</th>
                    <th>Status/Note</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php
                foreach ($assets as $asset) {
                    ?>
                    <tr class="tr-<?php // echo $asset->id   ?>">
                        <td><?php echo $asset->id ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            Name: <?php echo ucwords(strtolower($asset->name)) ?>
                             <hr>
                            Category: <?php echo ucwords(strtolower($asset->category)) ?>
                            <hr>
                            <img width="100px" src="<?php echo url('product_images/large/' . strtolower($asset->image)) ?>">
                        </td>
                        <td class="center units-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->units)) ?>
                        </td>
                        <td class="center value-<?php echo $asset->id ?>">
                            Per Unit Price: <?php echo ucwords(strtolower($asset->amount)) ?>
                            <hr>
                            Bulk price: <?php echo ucwords(strtolower($asset->bulkprice)) ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            Supplied By: <?php echo ucwords(strtolower($asset->supplier_name)) ?>
                            <hr>
                            Received By: <?php echo ucwords(strtolower($asset->received_by)) ?>
                             <hr>
                            Date: <?php echo ucwords(strtolower($asset->created_at)) ?>
                        </td>
                        <td class="center status-<?php echo $asset->id    ?>">
                            <?php echo ucwords(strtolower($asset->status))  ?>
                            <hr>
                            <?php echo ucwords(strtolower($asset->note))  ?>
                        </td>
                        <td> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>