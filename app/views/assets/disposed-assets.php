<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Asset Details</th>
                    <th>Disposal details</th>
                    <th>Note</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php foreach ($assets as $asset) { ?>
                    <tr class="tr-<?php echo $asset->id ?>">
                        <td><?php echo $asset->id ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            <?php echo '<strong>NAME:</strong></strong> ' . ucwords(strtolower($asset->name)) ?>
                            <?php echo '<hr><strong>CATEGORY: </strong>' . ucwords(strtolower($asset->category)) ?>
                            <?php echo '<hr><strong>BUYING PRICE: ' . ucwords(strtolower($asset->amount)) ?>
                        </td>
                        <td class="center name-<?php echo $asset->id ?>">
                            <?php
                            if ($asset->disposemethod == 0) {
                                $method = 'Donate';
                                $label = ' DISPOSAL COST';
                            } elseif ($asset->disposemethod == 1) {
                                $method = 'Throw Away';
                                $label = ' DISPOSAL COST';
                            } else {
                                $method = 'Sell';
                                $label = ' DISPOSAL PRICE';
                            };
                            echo '<strong>DISPOSAL METHOD:</strong> ' . $method;
                            echo '<hr><strong>' . $label . ':</strong> ' . ucwords(strtolower($asset->disposalamount));
                            ?>
                        </td>
                        <td class="center value-<?php echo $asset->id ?>">
                            <?php echo ucwords(strtolower($asset->note)); ?>
                        </td>
                        <td class="center status-<?php echo $asset->id ?>">
                            <?php
                            if ($asset->disposalstatus == 2) {
                                $label = ' Disposed';
                            } else {
                                $label = ' marked for disposal';
                            };
                            echo $label;

                            if ($asset->disposalstatus == 2) {
                                echo '<hr>';
                                echo $asset->names;
                                echo '<br>';
                                echo $asset->phone_no;
                                echo '<br>';
                                echo $asset->email;
                            }
                            ?>
                        </td>
                        <td>
                            <?php if ($asset->disposalstatus == 1) { ?>
                                <a href="javascript:;" class="btn btn-green btn-xs no-dispose-asset" data-id="<?php echo $asset->id ?>">
                                    Restore Asset
                                </a>
                                <a data-toggle="modal" href="#add" class="btn btn-green btn-xs mark-diposed" data-id="<?php echo $asset->id ?>">
                                    Mark as Disposed
                                </a>
                                <?php
                            } else {
                                echo '-';
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade modals" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Asset</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<span class='disposal-details' style='display:none'>
    <div class="panel-body collapse in">
        <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
        <div class="form-group cost">
            <?php echo Form::label('names', 'Names:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::text('names', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required', 'placeholder' => '')); ?>
            </div>
        </div>
        <div class="form-group cost">
            <?php echo Form::label('email', 'Email:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::text('email', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required', 'placeholder' => '')); ?>
            </div>
        </div>
        <div class="form-group cost">
            <?php echo Form::label('phone', 'Phone no:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::text('phone', $value = null, $attributes = array('class' => 'form-control', 'required' => 'required', 'placeholder' => '')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('name', 'Note:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo Form::textarea('note', $value = null, $attributes = array('class' => 'form-control', 'id' => 'note')); ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-disposed">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</span>
