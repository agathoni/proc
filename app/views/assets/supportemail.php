<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>AMS Support</title>


                <style type="text/css">
                    div,p,a,li,td{
                        -webkit-text-size-adjust:none;
                    }
                    #outlook a{
                        padding:0;
                    }
                    html{
                        width:100%;
                    }
                    body{
                        width:100% !important;
                        -webkit-text-size-adjust:100%;
                        -ms-text-size-adjust:100%;
                        margin:0;
                        padding:0;
                        font-family:Arial, Helvetica, sans-serif;
                    }
                    .ExternalClass{
                        width:100%;
                    }
                    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
                        line-height:100%;
                    }
                    .backgroundTable,backgroundTableFooter{
                        margin:0;
                        padding:0;
                        width:100% !important;
                        line-height:100% !important;
                    }
                    /*
                    @tab Page
                    @section background color
                    @tip Set the background color for your email.You may want to choose one that matches your company's branding
                    */
                    #MainTable{
                        /*@editable*/background-color:#E2E2EB;
                    }
                    /*
                    @tab Page
                    @section Content Background
                    @tip Set the background color for the content section of your email's body area.
                    @theme page
                    */
                    .backgroundTable{
                        /*@editable*/background-color:#FCFCFC;
                    }
                    /*
                    @tab Header
                    @section Preheader background
                    @tip Set the background color for your email's preheader area.
                    @theme page
                    */
                    .backgroundTableHeader{
                        /*@editable*/background-color:#F7F7F7;
                    }
                    /*
                    @tab Body
                    @section body text
                    @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
                    @theme main
                    */
                    .bodyContent div{
                        /*@editable*/color:#727272;
                        /*@editable*/font-family:Arial;
                        /*@editable*/font-size:15px;
                        /*@editable*/line-height:150%;
                        /*@editable*/text-align:left;
                    }
                    /*
                    @tab Body
                    @section body link
                    @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
                    */
                    .bodyContent div a:link,.bodyContent div a:visited{
                        /*@editable*/color:#056EB3;
                        /*@editable*/font-weight:normal;
                        /*@editable*/text-decoration:none;
                    }
                    /*
                    @tab Products
                    @section Product Details
                    @tip Set the styling for the details section of your product.
                    */
                    .productContent{
                        /*@editable*/background-color:#EDEDED;
                        /*@editable*/font-family:Arial;
                        /*@editable*/color:#056EB3;
                    }
                    /*
                    @tab Products
                    @section Product Title
                    @tip Set the styling for the Product title.
                    */
                    .productTitle,.productTitle a:link,.productTitle a:visited{
                        /*@editable*/font-size:16px;
                        /*@editable*/color:#000000;
                        /*@editable*/text-align:left;
                        /*@editable*/line-height:20px;
                    }
                    .OldPrice a:link{
                        font-size:14px;
                        color:#616161;
                        text-align:left;
                        line-height:20px;
                    }
                    .NewPrice a:link{
                        font-size:14px;
                        color:#F79B00;
                        text-align:left;
                        line-height:20px;
                    }
                    /*
                    @tab Products
                    @section Old Price
                    @tip Set the styling for the Old Price.
                    */
                    .OldPrice{
                        /*@editable*/font-size:14px;
                        /*@editable*/color:#616161;
                        /*@editable*/text-decoration:line-through;
                        /*@editable*/line-height:20px;
                    }
                    /*
                    @tab Products
                    @section New Price
                    @tip Set the styling for the Old Price.
                    */
                    .NewPrice{
                        /*@editable*/font-size:14px;
                        /*@editable*/color:#F79B00;
                        /*@editable*/text-align:left;
                        /*@editable*/line-height:20px;
                    }
                    /*
                    @tab Products
                    @section View deal link
                    @tip Set the styling for the View deal link.
                    */
                    .dealLink,.dealLink a:link,.dealLink a:visited{
                        /*@editable*/font-size:13px;
                        /*@editable*/line-height:17px;
                        /*@editable*/font-weight:normal;
                        /*@editable*/color:#056EB3;
                        /*@editable*/text-decoration:none;
                    }
                    .dealLink{
                        padding:0 16px 0 0;
                    }
                    /*
                    @tab Footer
                    @section Footer contacts & social Links Background
                    @tip Set the background color for thesection with contacts & social Links
                    @theme footer
                    */
                    .backgroundTableFooter{
                        /*@editable*/background:#333333;
                    }
                    img{
                        outline:none;
                        text-decoration:none;
                        border:none;
                        width:100%;
                        -ms-interpolation-mode:bicubic;
                    }
                    a img{
                        border:none;
                        width:100%;
                        display:block;
                    }
                    p{
                        margin:0px 0px !important;
                    }
                    table td{
                        border-collapse:collapse;
                    }
                    table{
                        border-collapse:collapse;
                        mso-table-lspace:0pt;
                        mso-table-rspace:0pt;
                    }
                    a{
                        color:#F79900;
                        text-decoration:none!important;
                    }
                    table[class=full]{
                        width:100%;
                        clear:both;
                    }
                    .info{
                        border-top-style:solid;
                        border-right-style:solid;
                        border-bottom-style:solid;
                        border-left-style:solid;
                        border-top-width:1px;
                        border-right-width:1px;
                        border-bottom-width:1px;
                        border-left-width:1px;
                        border-top-color:#B7D6EA;
                        border-right-color:#B7D6EA;
                        border-bottom-color:#B7D6EA;
                        border-left-color:#B7D6EA;
                    }
                    .b_right{
                        border-right-style:solid;
                        border-right-color:#C2C2C2;
                        border-right-width:1px;
                    }
                    .bannerBox{
                        border-style:solid;
                        border-width:1px;
                        border-color:#E4E4E4;
                    }
                    .bannerBox a{
                        display:block;
                        width:100%!important;
                    }
                    .MsoNormal{
                        background:#FFFFFF;
                    }
                    .lft-img{
                        float:left;
                        text-align:left;
                        width:30%;
                        padding:5px;
                        margin:10px;
                        margin-left:0px;
                    }
                    .lft-txt{
                        float:left;
                        text-align:left;
                        line-height:18px;
                        font-size:13px;
                        width:60%;
                        padding:5px;
                        margin:10px;
                        margin-right:0px;
                    }
                    .bxline{
                        border:1px solid #e9e9e9;
                        border-left:none;
                        border-right:none;
                        padding-right:50px;
                        text-align: right;
                    }
                    .pricing{
                        font-size:14px;
                        color:#F78609;
                        padding:2px 0;
                        font-weight:bold;
                    }
                    .alink{
                        font-size:13px;
                        color:#0A70B0;
                        padding:2px 0;
                        font-weight:bold;
                    }
                    .brline{
                        border:1px solid #e9e9e9;
                    }
                    .btn-reset{
                        font-size:18px;
                        font-weight:bold;
                        color:#fff;
                        padding:10px 15px;
                        margin:15px auto;
                        border-radius:2px;
                        border:1px solid #ccc;
                        width:auto;
                        cursor:pointer;
                        max-width:60%;
                        text-align:center;
                        background:linear-gradient(to bottom, #8dc63f 0%, #8dc63c 90%);
                    }
                    .btn-reset:hover{
                        opacity:0.9;
                        filter:alpha(opacity=90);
                    }
                    @media only screen and (max-width: 640px){
                        a[href^=tel],a[href^=sms]{
                            text-decoration:none;
                            color:#33b9ff;
                            cursor:default;
                        }

                    }	@media only screen and (max-width: 640px){
                        .mobile_link a[href^=tel],.mobile_link a[href^=sms]{
                            text-decoration:default;
                            color:#33b9ff !important;
                            pointer-events:auto;
                            cursor:default;
                        }

                    }	@media only screen and (max-width: 640px){
                        table[class=devicewidth]{
                            width:440px!important;
                            text-align:center!important;
                        }

                    }	@media only screen and (max-width: 640px){
                        table[class=devicewidthinner]{
                            width:420px!important;
                            text-align:center!important;
                        }

                    }	@media only screen and (max-width: 640px){
                        img[class=banner]{
                            width:440px!important;
                            height:220px!important;
                        }

                    }	@media only screen and (max-width: 640px){
                        img[class=colimg2]{
                            width:440px!important;
                            height:220px!important;
                        }

                    }	@media only screen and (max-width: 480px){
                        a[href^=tel],a[href^=sms]{
                            text-decoration:none;
                            color:#ffffff;
                            cursor:default;
                        }

                    }	@media only screen and (max-width: 480px){
                        .mobile_link a[href^=tel],.mobile_link a[href^=sms]{
                            text-decoration:default;
                            color:#ffffff !important;
                            pointer-events:auto;
                            cursor:default;
                        }

                    }	@media only screen and (max-width: 480px){
                        table[class=devicewidth]{
                            width:280px!important;
                            text-align:center!important;
                        }

                    }	@media only screen and (max-width: 480px){
                        table[class=devicewidthinner]{
                            width:260px!important;
                            text-align:center!important;
                        }

                    }	@media only screen and (max-width: 480px){
                        img[class=banner]{
                            width:280px!important;
                            height:140px!important;
                        }

                    }	@media only screen and (max-width: 480px){
                        img[class=colimg2]{
                            width:280px!important;
                            height:140px!important;
                        }

                    }	@media only screen and (max-width: 480px){
                        img[class=buttonlink]{
                            display:block;
                            margin-left:auto;
                            margin-right:auto;
                        }

                    }	@media only screen and (max-width: 480px){
                        .lft-img{
                            float:left;
                            text-align:left;
                            width:100%;
                            padding:5px;
                            margin:10px 0;
                        }

                    }	@media only screen and (max-width: 480px){
                        .lft-txt{
                            float:left;
                            font-size:13px;
                            line-height:18px;
                            text-align:left;
                            width:100%;
                            padding:5px;
                            margin:10px 0;
                        }

                    }	@media only screen and (max-width: 480px){
                        .btn-reset{
                            font-size:18px;
                            font-weight:bold;
                            color:#fff;
                            padding:10px 15px;
                            margin:15px auto;
                            border-radius:2px;
                            border:1px solid #ccc;
                            width:auto;
                            cursor:pointer;
                            max-width:90%;
                            text-align:center;
                            background:linear-gradient(to bottom, #8dc63f 0%, #8dc63c 90%);
                        }

                    }</style></head>
                <body>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" id="MainTable">
                        <tr>
                            <td>
                                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                    <tr>
                                        <td valign="top">

                                            <!-- Start of preheader -->
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTableHeader">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <!-- // Begin Template Preheader \\ -->
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                <tbody>
                                                                    <!-- // Begin Module: Standard Preheader \\ -->
                                                                    <tr>
                                                                        <!--<td align="center" valign="middle" style="font-family: arial, Helvetica, sans-serif; font-size: 13px;color: #727272;" mc:edit="std_preheader_links">&nbsp;&nbsp; *|IFNOT:ARCHIVE_PAGE|* Can't see this Email? View it in your <a href="*|ARCHIVE|*" style="text-decoration: none; color: #F78609;">Browser </a>  *|END:IF|*  &nbsp;&nbsp;</td>-->
                                                                    </tr>
                                                                    <!-- // End Module: Standard Preheader \\ -->
                                                                </tbody>
                                                            </table>
                                                            <!-- // End Template Preheader \\ -->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- End of preheader --> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">

                                            <!-- Start of header -->
                                            <table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table width="600" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                <tr> 
                                                                                                    <td>
                                                                                                        <!-- logo -->
                                                                                                        <table width="170" align="left" border="0" cellpadding="0" cellspacing="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td width="170" height="64" align="center" valign="bottom">
                                                                                                                        <div class="imgpop">
                                                                                                                            Support Email
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <!-- end of logo -->
                                                                                                    </td>
                                                                                                    <td>&nbsp;</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td width="170" height="17" align="center">
                                                                                                        <div class="imgpop">
                                                                                                            <img src="http://gallery.mailchimp.com/139f8fec3dec0d1e8f170c41e/images/6e5cc56b-3020-4cf7-bb1e-a9f7d13d3f63.png" alt="" border="0" width="170" height="17" style="display:block; border:none; outline:none; text-decoration:none;">
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <img src="http://gallery.mailchimp.com/139f8fec3dec0d1e8f170c41e/images/84715e6a-8a5a-4e85-9124-56c95741725d.png" alt="" border="0" width="100%" height="17" style="display:block; border:none; outline:none; text-decoration:none;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>                                 
                                                                                        </td>
                                                                                        <td>
                                                                                            <img src="http://gallery.mailchimp.com/139f8fec3dec0d1e8f170c41e/images/84715e6a-8a5a-4e85-9124-56c95741725d.png" alt="" border="0" width="100%" height="17" style="display:block; border:none; outline:none; text-decoration:none;">
                                                                                        </td>
                                                                                    </tr>                              
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- End of Header -->

                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="middle">
                                                                             
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="middle">
                                                                            <br/>
                                                                            <?php echo $msg ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable" mc:repeatable="">
                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                    </tr>                                        
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
                                                <tbody>
                                                    <!-- // Begin Module: Products Lists \\ -->
                                                    <tr>
                                                        <td valign="top" align="center">

                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#fff">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" align="left"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td valign="top">
                                            <!-- Start of Products List --> 
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable image_prod_fix">
                                                <tbody>
                                                    <!-- // Begin Module: Products Lists \\ -->
                                                    <tr mc:repeatable="">
                                                        <td>
                                                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                                <tbody>
                                                                    <!-- Spacing -->
                                                                    <tr>
                                                                        <td height="5"></td>
                                                                    </tr>
                                                                    <!-- Spacing -->
                                                                    <tr>
                                                                        <td>
                                                                            <table width="580" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" style="text-align: center; vertical-align: top; font-size: 0;">
                                                                                            <!-- spacing for mobile devices-->
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td width="100%" height="10" style="font-size:13px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <!-- end of for mobile devices-->
                                                                                            <div style=" height:10px; font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- Spacing -->
                                                                    <tr>
                                                                        <td height="5"></td>
                                                                    </tr>
                                                                    <!-- Spacing -->
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <!-- // End Module: Products Lists \\ -->

                                                    <!-- Start of View All Goods -->   
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <!-- End of View All Goods -->   
                                                </tbody>
                                            </table>
                                            <!-- End of Products List -->   
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign="top">
                                            <!-- Start of footer -->
                                            <table width="100%" border="0" bgcolor="#333333" cellspacing="0" cellpadding="0" id="backgroundTable">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table bgcolor="#333333" width="600" border="0" align="center" cellspacing="0" cellpadding="0" class="devicewidth">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table width="580" bgcolor="#333333" border="0" align="center" cellspacing="0" cellpadding="10" class="devicewidth">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="middle">
                                                                                            <!-- start of left column -->
                                                                                            <table width="280" border="0" align="left" cellspacing="0" cellpadding="0" class="devicewidth">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="280" border="0" align="center" cellspacing="0" cellpadding="0" class="devicewidth" style="font-family: arial, Helvetica, sans-serif; font-size: 13px;color: #FFFFFF;">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                    </tr>                                                  
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <!-- end of left column -->
                                                                                            <!-- spacing for mobile devices-->
                                                                                            <table border="0" align="left" cellspacing="0" cellpadding="0" class="mobilespacing">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <!-- end of for mobile devices-->
                                                                                            <!-- Start of Right column -->
                                                                                            <table width="200" border="0" align="right" cellspacing="0" cellpadding="0" class="devicewidth">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="center" colspan="8" width="100%" height="35" style="font-family: arial, Helvetica, sans-serif; font-size: 18px;color: #FFFFFF;"><strong>Get in touch</strong></td>
                                                                                                    </tr>
                                                                                                    <!-- image -->
                                                                                                    <tr>
                                                                                                    </tr>
                                                                                                    <!-- /image -->
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <!-- end of Right column -->

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- End of footer -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <!-- Start of Postfooter -->
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="middle" style="font-family: arial, Helvetica, sans-serif; font-size: 13px;color: #727272; line-height:21px" mc:edit="std_prefooter">
                                                            You can write to us: <a href="mailto:helpdesk@ams.com" target="_blank">helpdesk@ams.com</a> or call us on: <a href="tel:0727010778" value="+254700000" target="_blank">0700000</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="middle" style="font-family: arial, Helvetica, sans-serif; font-size: 13px;color: #727272; line-height:21px" mc:edit="std_footer_copyright">
                                                            &copy;2014 AMS Ltd. All rights reserved
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- End of postfooter -->  
                                        </td>
                                    </tr>
                                </table> 
                            </td>
                        </tr>
                    </table> 
                </body>
                </html>