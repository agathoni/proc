<div class="panel panel-sky">
    <div class="panel-body collapse in">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Details</th>
                    <th>Message</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody class="t-body">
                <?php
                foreach ($assets as $asset) {
//                    echo '<pre>';
//                    print_r($asset);
//                    echo '</pre>';
                    ?>
                    <tr class="tr-<?php echo $asset->id ?>">
                        <td class="center id-<?php echo $asset->id ?>"><?php echo $asset->id ?>
                        </td>
                        <td class="center date-<?php echo $asset->id ?>"><?php echo ucwords(strtolower($asset->created_at)) ?></td>
                        <td class="center details-<?php echo $asset->id ?>">
                            <?php
                            echo $asset->name;
                            echo '<hr>';
                            echo $asset->phone;
                            echo '<hr>';
                            echo $asset->email;
                            ?>
                        </td>
                        <td class="center message-<?php echo $asset->id ?>">
                            <?php
                            echo ucfirst($asset->subject);
                            echo '<hr>';
                            echo ucfirst($asset->message);
                            ?>
                        </td>
                        <td class="center status-<?php echo $asset->id ?>">
                            <a href="javascript:;" class="btn btn-xs <?php echo $asset->class ?>" >
                                <?php echo ucwords(strtolower($asset->status_label)) ?>
                            </a>
                            <br/>
                            <?php
                            if ($asset->assigned_to == '') {
                                ?>
                                <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs asign-support" data-id="<?php echo $asset->id ?>">Unassigned</a>
                                <?php
                            } else {
                                $user = DB::table('users')
                                        ->where('id', $asset->assigned_to)
                                        ->get();
                                ?>
                                <a data-toggle="modal" href="#add" class="btn btn-primary btn-xs re-assign-support" data-id="<?php echo $asset->id ?>"><?php echo "Assigned to " . $user[0]->name; ?></a>
                                <?php
                            }
                            echo '<br/>';
                            if ($asset->status == 3) {
                                $date = explode('-', $asset->updated_at);
                                $label1 = 'Processed in ';
                                $label2 = '';
                            } else {
                                $date = explode('-', $asset->created_at);
                                $label1 = 'Created ';
                                $label2 = 'ago';
                            }

                            $day = explode(' ', $date[2]);
                            $date[2] = $day[0];

                            $time_array = explode(':', $day[1]);
                            $time = mktime($time_array[0], $time_array[1], $time_array[2], $date[1], $date[2], $date[0]);
                            $f = \Carbon\Carbon::createFromTimestamp(time());
                            $t = \Carbon\Carbon::createFromTimestamp($time);
                            $days = $f->diffInDays($t);
                            $hours = $f->diffInHours($t);
                            $minutes = $f->diffInMinutes($t);

                            if ($days > 0) {
                                echo $label1 . $days . ' days ' . $label2;
                            }if ($hours > 0) {
                                echo $label1 . $hours . ' hours ' . $label2;
                            } else {
                                echo $label1 . $minutes . ' minutes ' . $label2;
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<span class='assign-form' style='display:none'>
    <div class="panel-body collapse in">
        <?php echo Form::model('Assetsmodel', array('class' => 'form form-horizontal row-border', 'method' => 'post')); ?>
        <div class="form-group">
            <?php echo Form::label('user', 'User:', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php
                echo Form::select('assignuser_id', $users, null, $attributes = array('class' => 'form-control', 'required' => 'required', 'id' => 'assignuser_id'))
                ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn save-assign-asset">Submit</button>
                        <a class="btn-default btn btn-cancel close-modal" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</span>