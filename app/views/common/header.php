<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Asset Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Avant">
        <meta name="author" content="The Red Team">

        <link href="<?php echo url() ?>/assets/less/styles.less" rel="stylesheet/less" media="all"> 
        <!-- <link rel="stylesheet" href="assets/css/styles.css?=121"> -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

        <?php
        if (isset($_COOKIE["theme"])) {
            echo "<link href='assets/demo/variations/" . $_COOKIE["theme"] . "' rel='stylesheet' type='text/css' media='all' id='styleswitcher'>";
        } else {
            ?> 
            <link href='<?php echo url() ?>/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher'> 
        <?php } ?>

        <?php
        if (isset($_COOKIE["headerstyle"])) {
            echo "<link href='assets/demo/variations/" . $_COOKIE["headerstyle"] . "' rel='stylesheet' type='text/css' media='all' id='headerswitcher'>";
        } else {
            ?>
            <link href='<?php echo url() ?>/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher'> 
        <?php } ?>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/ie8.css">
                <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
        <![endif]-->

        <!-- The following CSS are included as plugins and can be removed if unused-->

        <?php

        function linktag($address) {
            $address = url() . '/' . $address;
            echo "<link rel='stylesheet' type='text/css' href='$address' /> \n";
        }

        if (isset($css)) {
            foreach ($css as $filepath) {
                linktag($filepath);
            }
        }

        $pageName = basename($_SERVER['PHP_SELF']);
        if ($pageName == "maps-vector.php") {
            linktag('assets/plugins/jqvmap/jqvmap.css'); // <!-- jQuery vector maps -->
        } elseif ($pageName == "icons-glyphicons.php") {
            linktag('assets/fonts/glyphicons/css/glyphicons.min.css'); // <!-- glyphicons -->
        } elseif ($pageName == "form-components.php") {
            linktag('assets/plugins/form-select2/select2.css'); //<!-- Select2 -->
            linktag('assets/plugins/form-multiselect/css/multi-select.css'); //<!-- Multiselect -->
            linktag('assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.css'); //<!-- jq UI Timepicker -->
            linktag('assets/plugins/form-daterangepicker/daterangepicker-bs3.css'); //<!-- DateRangePicker -->
            linktag('assets/plugins/form-fseditor/fseditor.css'); //<!-- FullScreen Editor -->
            linktag('assets/plugins/form-tokenfield/bootstrap-tokenfield.css'); //<!-- Tokenfield -->
            linktag('assets/js/jqueryui.css'); // <!-- jquery ui -->
        } elseif ($pageName == "ui-paginations.php") {
            linktag('assets/plugins/datepaginator/bootstrap-datepaginator.css');
        } elseif ($pageName == "charts-svg.php") {
            linktag('assets/plugins/charts-morrisjs/morris.css'); //> <!-- Charts -->
        } elseif ($pageName == "form-fileupload.php") {
            linktag('assets/plugins/jquery-fileupload/css/jquery.fileupload-ui.css'); // <!--File Upload-->
        } elseif ($pageName == "form-dropzone.php") {
            linktag('assets/plugins/dropzone/css/dropzone.css'); //<!-- Dropzone-->
        } elseif ($pageName == "ui-tour.php") {
            linktag('assets/plugins/bootstro.js/bootstro.min.css'); //<!-- Bootstro.js-->
        } elseif ($pageName == "ui-sliders.php") {
            linktag('assets/plugins/progress-skylo/skylo.css'); // <!-- Sky Loader Progress Bar -->
            linktag('assets/js/jqueryui.css'); // <!-- jquery ui -->
        } elseif ($pageName == "form-imagecrop.php") {
            linktag('assets/plugins/jcrop/css/jquery.Jcrop.min.css'); // <!-- jCrop -->
        } elseif ($pageName == "index.php") {
            linktag('assets/plugins/form-daterangepicker/daterangepicker-bs3.css'); //<!-- DateRangePicker -->
            linktag('assets/plugins/fullcalendar/fullcalendar.css'); // <!-- Calendar -->
            linktag('assets/plugins/form-markdown/css/bootstrap-markdown.min.css');
        } elseif ($pageName == "calendar.php") {
            linktag('assets/plugins/fullcalendar/fullcalendar.css'); // <!-- Calendar -->
        } elseif ($pageName == "ui-nestable.php") {
            linktag('assets/plugins/form-nestable/jquery.nestable.css'); //<!-- Nestable Lists-->
        } elseif ($pageName == "tables-data.php") {
            linktag('assets/plugins/datatables/dataTables.css'); // <!-- Data Tables -->
        } elseif ($pageName == "tables-editable.php") {
            linktag('assets/plugins/datatables/dataTables.css'); // <!-- Data Tables -->
        } elseif ($pageName == "form-xeditable.php") {
            linktag('assets/plugins/form-xeditable/bootstrap3-editable/css/bootstrap-editable.css'); // <!-- X-Editable -->
        } elseif ($pageName == "ui-alerts.php") {
            linktag('assets/plugins/pines-notify/jquery.pnotify.default.css');
        }
        linktag('assets/plugins/codeprettifier/prettify.css'); // <!-- Google Code Prettifier -->
        linktag('assets/plugins/form-toggle/toggles.css'); //<!-- Toggles -->
        linktag('assets/css/jquery.fancybox.css'); //<!-- Toggles -->
        ?>

        <script type="text/javascript" src="<?php echo url() ?>/assets/js/less.js"></script>
    </head>

    <body class="<?php
    if (isset($_COOKIE["admin_leftbar_collapse"]))
        echo ($_COOKIE['admin_leftbar_collapse'] . " "); // check collapse state with php
    if (isset($_COOKIE["admin_rightbar_show"]))
        echo $_COOKIE['admin_rightbar_show'];
    if (isset($_COOKIE["fixed-header"]))
        echo ' static-header';
    ?>">



        <header class="navbar navbar-inverse <?php
        if (isset($_COOKIE["fixed-header"])) {
            echo 'navbar-static-top';
        } else {
            echo 'navbar-fixed-top';
        }
        ?>" role="banner">
            <a id="leftmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="right" title="Toggle Sidebar"></a>

            <div class="navbar-header pull-left">
            </div>
            <?php if (Auth::check()) { ?>
                <ul class="nav navbar-nav pull-right toolbar">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle username" data-toggle="dropdown"><span class="hidden-xs"><?php echo Auth::user()->name; ?> <i class="fa fa-caret-down"></i></span><img src="assets/demo/avatar/dangerfield.png" alt="Dangerfield" /></a>
                        <ul class="dropdown-menu userinfo arrow">
                            <li class="userlinks">
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo url('edit-user/' . Auth::user()->id) ?>">Edit Profile <i class="pull-right fa fa-pencil"></i></a></li>
                                    <li><a href="<?php echo url('assets/my-assets') ?>">My assets <i class="pull-right fa fa-cog"></i></a></li>
                                    <!--<li><a href="#">Help <i class="pull-right fa fa-question-circle"></i></a></li>-->
                                    <li class="divider"></li>
                                    <li><a href="<?php echo url('logout') ?>" class="text-right">Sign Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
<!--                    <li class="dropdown">
                        <a href="#" class="hasnotifications dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list-ol"></i>
                            <span class="badge">3</span>
                        </a>
                    </li>-->
                </ul>
            <?php } ?>
        </header>

        <div id="page-container">
            <!-- BEGIN SIDEBAR -->
            <nav id="page-leftbar" role="navigation">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="acc-menu" id="sidebar">
                    <!--                    <li id="search">
                                            <a href="javascript:;"><i class="fa fa-search opacity-control"></i></a>
                                            <form>
                                                <input type="text" class="search-query" placeholder="Search...">
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </form>
                                        </li>-->
                    <li class="divider"></li>
                    <li><a href="<?php echo url('') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    <?php
                    $modules = explode(',', Auth::user()->modules);
                    if (in_array(1, $modules)) {
                        echo View::make('users/side-menu');
                    }
                    if (in_array(2, $modules)) {
                        echo View::make('assetcat/side-menu');
                    }
                    if (in_array(3, $modules)) {
                        echo View::make('assets/side-menu');
                    }
                    if (in_array(5, $modules)) {
                        echo View::make('suppliers/side-menu');
                    }
                    ?>
                    <?php // echo View::make('assetcat/side-menu'); ?>
                    <?php // echo View::make('assets/side-menu'); ?>
                    <li class=""><a href="javascript:;"><i class="fa fa-th"></i> <span>Reports</span> </a>
                        <ul class="acc-menu">
                            <li class=""><a href="javascript:;"><i class="fa fa-th"></i> <span>Inactive Records</span> </a>
                                <ul class="acc-menu">
                                    <li><a href="<?php echo url('users/deleted-users') ?>"><span>Inactive Users</span></a></li>
                                    <li><a href="<?php echo url('categories/deleted-categories') ?>"><span>Inactive Categories</span></a></li>
                                    <li><a href="<?php echo url('assets/deleted-assets') ?>"><span>Inactive Assets</span></a></li>
                                </ul>
                        </ul>
                    </li>
                </ul>
                <!-- END SIDEBAR MENU -->
            </nav>

            <!-- BEGIN RIGHTBAR -->
            <!-- END RIGHTBAR -->