<div id="page-heading">
    <h1>Dashboard</h1>
    <div class="options">
        <div class="btn-toolbar">
            <button class="btn btn-default" id="daterangepicker2">
                <i class="fa fa-calendar-o"></i> 
                <span class="hidden-xs hidden-sm">December 16, 2014 - January 15, 2015</span>
            </button>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3 col-xs-12 col-sm-6">
                    <a class="info-tiles tiles-toyo" href="<?php echo url('users') ?>">
                        <div class="tiles-heading">Memebers</div>
                        <div class="tiles-body-alt">
                            <!--i class="fa fa-bar-chart-o"></i-->
                            <div class="text-center"><span class="text-top"></span><?php echo $summary['users'] ?></div>
                        </div>
                        <div class="tiles-footer">go to accounts</div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-6">
                    <a class="info-tiles tiles-success" href="<?php echo url('assets/active-assets') ?> ">
                        <div class="tiles-heading">Assets</div>
                        <div class="tiles-body-alt">
                            <!--i class="fa fa-money"></i-->
                            <div class="text-center"><span class="text-top"></span><?php echo $summary['assets'] ?></div>
                        </div>
                        <div class="tiles-footer">manage members</div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-6">
                    <a class="info-tiles tiles-orange" href="<?php echo url('assets/disposed-assets') ?>">
                        <div class="tiles-heading">Disposable Assets</div>
                        <div class="tiles-body-alt">
                            <i class="fa fa-group"></i>
                            <div class="text-center"><?php echo $summary['disp-assets'] ?></div>
                            <small>Assets marked for disposal</small>
                        </div>
                        <div class="tiles-footer">manage disposable assets</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>