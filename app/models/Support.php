<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Support extends Eloquent implements  RemindableInterface {

    use  RemindableTrait;

  
    protected $table = 'support';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
     protected $fillable = array('user_id','asset_id','message');

}
