<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Assetstatusmodel extends Eloquent implements RemindableInterface {

    use RemindableTrait;

    protected $table = 'asset_status';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name');

}
