<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class DisposalDetails extends Eloquent implements RemindableInterface {

    use RemindableTrait;

    protected $table = 'disposal_details';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('disposed_asset_id', 'names', 'email', 'phone_no', 'note');

}
