<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Assetsmodel extends Eloquent implements  RemindableInterface {

    use  RemindableTrait;

  
    protected $table = 'assets';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
     protected $fillable = array('image','ar_id','name','category_id','received_by','status','note','amount','disposed','assigned');

}
