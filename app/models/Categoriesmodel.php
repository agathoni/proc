<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Categoriesmodel extends Eloquent implements  RemindableInterface {

    use  RemindableTrait;

  
    protected $table = 'asset_categories';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
     protected $fillable = array('name');

}
