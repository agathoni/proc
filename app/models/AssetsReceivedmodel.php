<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AssetsReceivedmodel extends Eloquent implements  RemindableInterface {

    use  RemindableTrait;

  
    protected $table = 'assets_received';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
     protected $fillable = array('image','name','category_id','received_by','status','note','amount','units','supplier_id','bulkprice');

}
