<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Assetsassignmentmodel extends Eloquent implements RemindableInterface {

    use RemindableTrait;

    protected $table = 'asset-assignment';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('asset_id', 'user_id');

}
