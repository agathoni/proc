<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AssetDisposal extends Eloquent implements  RemindableInterface {

    use  RemindableTrait;

  
    protected $table = 'asset-disposal';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
     protected $fillable = array('disposemethod','amount','note','status','asset_id');

}
