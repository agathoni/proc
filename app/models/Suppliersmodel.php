<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Suppliersmodel extends Eloquent implements  RemindableInterface {

    use RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'suppliers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    protected $fillable = array('name', 'email', 'phone');

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

}
